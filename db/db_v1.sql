-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 08 Jul 2018 pada 08.17
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_etugas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `user` tinytext NOT NULL,
  `pass` tinytext NOT NULL,
  `nama` tinytext,
  `tingkat` tinytext NOT NULL,
  `pelajaran` tinytext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `user`, `pass`, `nama`, `tingkat`, `pelajaran`) VALUES
(1, 'admin', 'admin', 'tuan admin baru', '3', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(11) NOT NULL,
  `username` tinytext NOT NULL,
  `nama` tinytext NOT NULL,
  `pass` text NOT NULL,
  `id_kelompok` int(12) NOT NULL,
  `foto` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`id_guru`, `username`, `nama`, `pass`, `id_kelompok`, `foto`) VALUES
(57, 'fahmi', 'Muhammad Fahmi', 'fahmi', 1, 'Selection_001.png'),
(58, 'munir', 'Misbahul Munir', 'munir', 6, 'Selection_002.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` int(3) NOT NULL,
  `jurusan` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `jurusan`) VALUES
(1, 'Matematika'),
(2, 'Biologi'),
(3, 'Kimia'),
(4, 'Fisika');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelompok`
--

CREATE TABLE `kelompok` (
  `id_kelompok` int(3) NOT NULL,
  `kelompok` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelompok`
--

INSERT INTO `kelompok` (`id_kelompok`, `kelompok`) VALUES
(1, '1. Kucing'),
(2, '2. Kuda'),
(3, '3. Kambing'),
(4, '4. Gajah'),
(5, '5. Kerbau'),
(6, '6. Banteng');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kotak_masuk`
--

CREATE TABLE `kotak_masuk` (
  `id_kotakmasuk` int(11) NOT NULL,
  `m_file` text,
  `nis` text,
  `nama_siswa` text,
  `id_kelompok` int(12) DEFAULT NULL,
  `jurusan` int(30) NOT NULL,
  `tahun_angkatan` varchar(30) NOT NULL,
  `nama_guru` text,
  `keterangan` int(3) NOT NULL COMMENT '0 = Belum_verifikasi ; 1 = verifikasi',
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `judul` text NOT NULL,
  `judul_tugas` varchar(200) NOT NULL,
  `pokok_1` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kotak_masuk`
--

INSERT INTO `kotak_masuk` (`id_kotakmasuk`, `m_file`, `nis`, `nama_siswa`, `id_kelompok`, `jurusan`, `tahun_angkatan`, `nama_guru`, `keterangan`, `tanggal`, `judul`, `judul_tugas`, `pokok_1`) VALUES
(51, '2581-4909-1-PB.pdf', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-09 18:32:30', '', 'judul 1', 'pokok 1'),
(52, '2581-4909-1-PB.pdf', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-09 18:32:49', '', 'judul 1 ', 'pokok'),
(53, 'TEST AJA.pdf', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-10 05:39:16', '', 'Teknik editing foto menggunakan adobe phosotshop', 'Siswa dapat mengenal fungsi dan tool yang ada di adobe photoshop'),
(54, '', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-10 07:10:06', '', 'uu', 'iii'),
(55, '2581-4909-1-PB.pdf', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-10 07:10:33', '', 't', 'ytu'),
(56, '2581-4909-1-PB.pdf', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-10 07:57:47', '', '76576', '5757'),
(57, '2581-4909-1-PB.pdf', '1', 'siswa', 0, 0, '2015 - 2016', 'guru2', 0, '2016-11-10 07:58:14', '', '666', '6666'),
(58, 'BAB I.docx', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-10 09:01:09', '', '7676', '76777'),
(59, 'BAB IV.docx', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-10 09:01:43', '', '66', '777'),
(60, 'BAB III.docx', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-10 09:03:32', '', 't1', 't2'),
(61, 'TEST AJA.pdf', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-10 09:03:46', '', 'Teknik editing foto menggunakan adobe photoshop', 'Siswa dapat mengenal fungsi dan tool yang ada didalam adobe photoshop'),
(62, 'coba.docx', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-10 09:04:29', '', 'c1', 'c2'),
(63, 'TEST_AJA.docx', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-10 10:06:10', '', 'Teknik editing video menggunakan adobe phosotshop', 'Siswa dapat mengenal fungsi dan tool yang ada di adobe premier'),
(64, '', '1', 'siswa', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-11 10:02:24', '', 'Pembuatan Judul Makalah', 'Siswa paham maksud dari judul makalah'),
(65, 'TEST_AJA.docx', '1', 'siswa 10', 0, 0, '2015 - 2016', 'guru', 0, '2016-11-12 04:53:58', '', 'Buat aja makalah nya', 'kahda'),
(66, '', '1', 'Ali', 1, 0, '2015 - 2016', 'guru', 1, '2018-07-06 08:30:26', '', '', ''),
(69, '', '14650001', 'iqbal', 6, 0, '2017', 'fahmi', 0, '2018-07-08 04:22:43', '', 'cinta adalah buta', '<p>UIN Malang</p>\n'),
(70, 'Selection_001.png', '14650001', 'iqbal', 1, 0, '2017', 'fahmi', 1, '2018-07-08 04:25:35', '', 'assets/images/kirim_tugas', '<p>assets/images/kirim_tugas</p>\n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengelompokan`
--

CREATE TABLE `pengelompokan` (
  `id` int(3) NOT NULL,
  `nim` int(3) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `id_kelompok` int(3) DEFAULT NULL,
  `id_pendamping` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `nis` text,
  `nama` tinytext NOT NULL,
  `jurusan` tinytext NOT NULL,
  `kelas` tinytext NOT NULL,
  `tahunajaran` tinytext,
  `jenis_kelamin` tinytext,
  `foto` text,
  `password` text,
  `id_kelompok` int(12) NOT NULL,
  `id_pendamping` int(12) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nis`, `nama`, `jurusan`, `kelas`, `tahunajaran`, `jenis_kelamin`, `foto`, `password`, `id_kelompok`, `id_pendamping`) VALUES
(85, '14650001', 'iqbal', 'Fisika', '2. Kuda', '2017', 'Perempuan', 'Selection_006.png', 'iqbal', 6, 58),
(87, '14650002', 'Muslim', 'Matematika', '1. Kucing', '2017', 'Laki - Laki', 'Selection_001.png', 'muslim', 1, 57);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun`
--

CREATE TABLE `tahun` (
  `id_tahun` int(3) NOT NULL,
  `tahun` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tahun`
--

INSERT INTO `tahun` (`id_tahun`, `tahun`) VALUES
(1, '2017'),
(3, '2018');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_matapelajaran`
--

CREATE TABLE `tbl_matapelajaran` (
  `id_matapelajaran` int(11) NOT NULL,
  `matapelajaran` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_matapelajaran`
--

INSERT INTO `tbl_matapelajaran` (`id_matapelajaran`, `matapelajaran`) VALUES
(2, 'BHS'),
(3, 'TIK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tugas`
--

CREATE TABLE `tbl_tugas` (
  `id_tugas` int(23) NOT NULL,
  `judul` text,
  `nama_guru` text,
  `id_kelompok` text,
  `tgl` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `m_file` text,
  `berlaku` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_tugas`
--

INSERT INTO `tbl_tugas` (`id_tugas`, `judul`, `nama_guru`, `id_kelompok`, `tgl`, `m_file`, `berlaku`) VALUES
(5, '546', '64', '46', '0000-00-00 00:00:00', '464', '0000-00-00'),
(6, NULL, NULL, NULL, '2016-11-21 11:14:02', NULL, NULL),
(27, '98988', 'gurus', '0', '2009-11-15 17:00:00', '2581-4909-1-PB.pdf', '0000-00-00'),
(31, 'Buat makalah pembahasannya mengenai teknik editing foto menggunakan adobe lightroom', 'guru2', '0', '2011-11-15 17:00:00', 'TEST_AJA.pdf', '2009-11-19'),
(32, 'judul 6', '0', '2', '2011-11-15 17:00:00', '2581-4909-1-PB.pdf', '0000-00-00'),
(37, '33', 'qw', '1', '2007-07-17 17:00:00', '88360-ID-sistem-pendukung-keputusan-untuk-menentu.pdf', '2018-07-04'),
(38, 'iqbl fauzi', 'munir', '6', '2008-07-17 17:00:00', '88360-ID-sistem-pendukung-keputusan-untuk-menentu.pdf', '2018-07-09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `kelompok`
--
ALTER TABLE `kelompok`
  ADD PRIMARY KEY (`id_kelompok`);

--
-- Indexes for table `kotak_masuk`
--
ALTER TABLE `kotak_masuk`
  ADD PRIMARY KEY (`id_kotakmasuk`);

--
-- Indexes for table `pengelompokan`
--
ALTER TABLE `pengelompokan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`id_tahun`);

--
-- Indexes for table `tbl_matapelajaran`
--
ALTER TABLE `tbl_matapelajaran`
  ADD PRIMARY KEY (`id_matapelajaran`);

--
-- Indexes for table `tbl_tugas`
--
ALTER TABLE `tbl_tugas`
  ADD PRIMARY KEY (`id_tugas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kelompok`
--
ALTER TABLE `kelompok`
  MODIFY `id_kelompok` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kotak_masuk`
--
ALTER TABLE `kotak_masuk`
  MODIFY `id_kotakmasuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `pengelompokan`
--
ALTER TABLE `pengelompokan`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `tahun`
--
ALTER TABLE `tahun`
  MODIFY `id_tahun` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_matapelajaran`
--
ALTER TABLE `tbl_matapelajaran`
  MODIFY `id_matapelajaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_tugas`
--
ALTER TABLE `tbl_tugas`
  MODIFY `id_tugas` int(23) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
