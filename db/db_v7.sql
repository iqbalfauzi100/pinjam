-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 06 Agu 2018 pada 01.17
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_etugas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akses`
--

CREATE TABLE `akses` (
  `id` int(11) NOT NULL,
  `username` text,
  `password` text,
  `idLevel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akses`
--

INSERT INTO `akses` (`id`, `username`, `password`, `idLevel`) VALUES
(1, 'admin', 'admin', 1),
(2, 'tugasu', 'tugasu', 2),
(9, '14650001', '14650001', 6),
(14, 'humaniora', 'humaniora', 4),
(18, 'pendampingu', 'pendampingu', 3),
(19, '14650002', '14650002', 6),
(21, '14650003', '14650003', 6),
(22, '14650004', '14650004', 6),
(23, 'fahmi', 'fahmi', 3),
(27, 'hum1', 'hum1', 5),
(28, 'hum2', 'hum2', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `fakultas`
--

CREATE TABLE `fakultas` (
  `id` int(11) NOT NULL,
  `namaFk` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fakultas`
--

INSERT INTO `fakultas` (`id`, `namaFk`) VALUES
(1, 'Tarbiyah dan Ilmu Keguruan'),
(2, 'Syari’ah'),
(3, 'Humaniora'),
(4, 'Psikologi'),
(5, 'Ekonomi'),
(6, 'Sains dan Teknologi'),
(7, 'Kedokteran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hp`
--

CREATE TABLE `hp` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hp`
--

INSERT INTO `hp` (`id`, `nama`, `no`) VALUES
(1, 'Fahmi', '085814294695'),
(2, 'Iqbal', '085231633962'),
(3, 'Muslim', '085735307194');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `jurusan` text NOT NULL,
  `idFk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `jurusan`, `idFk`) VALUES
(1, 'Matematika', 6),
(2, 'Biologi', 6),
(3, 'Bahasa Sastra Inggris', 3),
(4, 'Hukum Bisnis Syariah', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelompok`
--

CREATE TABLE `kelompok` (
  `id_kelompok` int(3) NOT NULL,
  `kelompok` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelompok`
--

INSERT INTO `kelompok` (`id_kelompok`, `kelompok`) VALUES
(1, '1. Kucing'),
(2, '2. Kuda'),
(3, '3. Kambing'),
(4, '4. Gajah'),
(5, '5. Kerbau'),
(6, '6. Beruang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelompok_fk`
--

CREATE TABLE `kelompok_fk` (
  `id` int(11) NOT NULL,
  `namaKelompok` text,
  `idFk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelompok_fk`
--

INSERT INTO `kelompok_fk` (`id`, `namaKelompok`, `idFk`) VALUES
(1, 'kelompok 1', 3),
(2, 'kelompok 2', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kotak_masuk`
--

CREATE TABLE `kotak_masuk` (
  `id_kotakmasuk` int(11) NOT NULL,
  `id_tugas` int(11) NOT NULL,
  `nis` text,
  `nama_siswa` text,
  `jurusan` int(30) NOT NULL,
  `tahun_angkatan` varchar(30) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_file` text,
  `judul` text NOT NULL,
  `judul_tugas` text NOT NULL,
  `pokok_1` text NOT NULL,
  `nilai` int(3) NOT NULL COMMENT 'Nilai antara 1-100',
  `keterangan` int(3) NOT NULL COMMENT '0 = Belum_verifikasi ; 1 = verifikasi',
  `status_tugas` int(11) DEFAULT '0' COMMENT '0 = Universitas; selain itu untuk fakultas',
  `id_kelompok` int(12) DEFAULT NULL,
  `nama_guru` text,
  `idKelompokFk` int(11) DEFAULT NULL,
  `idPendampungFk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kotak_masuk`
--

INSERT INTO `kotak_masuk` (`id_kotakmasuk`, `id_tugas`, `nis`, `nama_siswa`, `jurusan`, `tahun_angkatan`, `tanggal`, `m_file`, `judul`, `judul_tugas`, `pokok_1`, `nilai`, `keterangan`, `status_tugas`, `id_kelompok`, `nama_guru`, `idKelompokFk`, `idPendampungFk`) VALUES
(126, 42, '14650001', 'alek', 3, '2018', '2018-07-26 06:45:08', '88360-ID-sistem-pendukung-keputusan-untuk-menentu.pdf', '', 'Indonesia', '<p>ini adalah itu</p>\r\n', 24, 1, 0, 1, '3', NULL, NULL),
(127, 43, '14650001', 'alek', 3, '2018', '2018-07-26 06:46:13', '88360-ID-sistem-pendukung-keputusan-untuk-menentu.pdf', '', 'Rangkuman Pondok Pesantren', '<p>contoh harga</p>\r\n', 12, 1, 0, 1, '3', NULL, NULL),
(133, 41, '14650001', 'alek', 3, '2018', '2018-08-05 03:38:39', '', '', 'Beasiswa', 'ok', 5, 0, 0, 1, '3', NULL, NULL),
(137, 43, '14650002', 'muslim', 2, '2018', '2018-08-05 13:18:35', '', '', 'Rangkuman Pondok Pesantren', 'muhammad muslim', 12, 0, 0, 1, '3', NULL, NULL),
(139, 44, '14650001', 'alek', 3, '2018', '2018-08-05 16:25:40', '', '', 'Basmallah', 'cobalah mengerti', 777, 1, 3, NULL, NULL, 1, 101),
(140, 44, '14650002', 'muslim', 3, '2018', '2018-08-05 22:58:21', '', '', 'Basmallah', 'ini muhammad muslim', 777, 0, 3, NULL, NULL, 1, 101);

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id`, `level`) VALUES
(1, 'Admin'),
(2, 'PenugasanUniversitas'),
(3, 'PendampingUniversitas'),
(4, 'PenugasanFakultas'),
(5, 'PendampingFakultas'),
(6, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nim` int(30) DEFAULT NULL,
  `nama` text,
  `tahun` tinytext,
  `jenis_kelamin` tinytext,
  `foto` text,
  `idJurusan` int(11) DEFAULT NULL,
  `idAkses` int(11) DEFAULT NULL,
  `idKelompokU` int(11) DEFAULT '0',
  `idPendampungU` int(11) DEFAULT '0',
  `idKelompokFk` int(11) DEFAULT '0',
  `idPendampungFk` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nim`, `nama`, `tahun`, `jenis_kelamin`, `foto`, `idJurusan`, `idAkses`, `idKelompokU`, `idPendampungU`, `idKelompokFk`, `idPendampungFk`) VALUES
(16, 14650001, 'alek', '2018', 'Perempuan', 'iwan1.jpg', 3, 9, 1, 3, 1, 101),
(17, 14650002, 'muslim', '2018', 'Laki - Laki', 'itsnasyahadah-20180716-0001.jpg', 3, 19, 1, 3, 1, 101),
(18, 14650003, 'fauzi', '2018', 'Laki - Laki', 'menaralangitan-20180713-0001.jpg', 3, 21, 0, 0, 1, 101),
(21, 14650020, 'fauzi', '2018', 'Perempuan', 'itsnasyahadah-20180716-0001.jpg', 2, 23, 0, 0, 2, 102),
(22, 14650004, 'Muhammad Ichwanuddin', '2017', 'Perempuan', '36660887_240285363454125_5712164189938647040_n.jpg', 3, 22, 0, 0, 2, 102);

-- --------------------------------------------------------

--
-- Stand-in structure for view `nilai_mahasiswa`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `nilai_mahasiswa` (
`nis` text
,`niss` text
,`nama_mhs` text
,`id_tugas` int(11)
,`judul_tugas` text
,`nilai` int(3)
,`status_tugas` int(11)
,`idJurusan` int(30)
,`jurusan` text
,`idKelompokUniv` int(3)
,`namaKelompokUniv` varchar(20)
,`idKelompokFk` int(11)
,`namaKelompokFk` text
,`idPendampingUniv` int(11)
,`namaPendampingUniv` tinytext
,`idPendampingFk` int(11)
,`namaPendampingFk` tinytext
,`total_nilai` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendamping_fk`
--

CREATE TABLE `pendamping_fk` (
  `id` int(11) NOT NULL,
  `idAkses` int(11) DEFAULT NULL,
  `nama` tinytext,
  `idKelompokFk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pendamping_fk`
--

INSERT INTO `pendamping_fk` (`id`, `idAkses`, `nama`, `idKelompokFk`) VALUES
(101, 27, 'hum1', 1),
(102, 28, 'hum2', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendamping_univ`
--

CREATE TABLE `pendamping_univ` (
  `id` int(11) NOT NULL,
  `idAkses` int(11) DEFAULT NULL,
  `nama` tinytext NOT NULL,
  `idKelompok` int(12) DEFAULT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pendamping_univ`
--

INSERT INTO `pendamping_univ` (`id`, `idAkses`, `nama`, `idKelompok`, `foto`) VALUES
(3, 18, 'Muh Fahmi Ferdiansyah', 1, 'iwan.jpg'),
(4, 23, 'ok', 2, 'iwan.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penugasan_fk`
--

CREATE TABLE `penugasan_fk` (
  `id` int(11) NOT NULL,
  `idAkses` int(11) DEFAULT NULL,
  `idFk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penugasan_fk`
--

INSERT INTO `penugasan_fk` (`id`, `idAkses`, `idFk`) VALUES
(6, 14, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penugasan_univ`
--

CREATE TABLE `penugasan_univ` (
  `id` int(11) NOT NULL,
  `idAkses` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penugasan_univ`
--

INSERT INTO `penugasan_univ` (`id`, `idAkses`) VALUES
(1, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting_sistem`
--

CREATE TABLE `setting_sistem` (
  `id` int(11) NOT NULL,
  `fitur` text NOT NULL,
  `status` int(3) NOT NULL COMMENT '0 = Tidak Aktif ; 1 = Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `setting_sistem`
--

INSERT INTO `setting_sistem` (`id`, `fitur`, `status`) VALUES
(1, 'Tambah Anggota Kelompok (Laman Pendamping - UNIVERSITAS)', 0),
(2, 'Tambah Anggota Kelompok (Laman Pendamping - FAKULTAS)', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun`
--

CREATE TABLE `tahun` (
  `id_tahun` int(3) NOT NULL,
  `tahun` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tahun`
--

INSERT INTO `tahun` (`id_tahun`, `tahun`) VALUES
(1, '2017'),
(3, '2018');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tugas`
--

CREATE TABLE `tbl_tugas` (
  `id_tugas` int(23) NOT NULL,
  `judul` text,
  `tgl` date DEFAULT NULL,
  `m_file` text,
  `berlaku` datetime DEFAULT NULL,
  `nilai` int(3) NOT NULL,
  `keterangan` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_tugas`
--

INSERT INTO `tbl_tugas` (`id_tugas`, `judul`, `tgl`, `m_file`, `berlaku`, `nilai`, `keterangan`) VALUES
(40, 'Pancasila', '2018-07-09', 'beasiswa.pdf', '2018-08-30 08:59:00', 10, 0),
(41, 'Beasiswa', '2018-07-09', 'topsis.pdf', '2018-08-09 08:50:00', 5, 0),
(42, 'Indonesia', '2018-07-09', '88360-ID-sistem-pendukung-keputusan-untuk-menentu.pdf', '2018-07-31 20:00:00', 24, 0),
(43, 'Rangkuman Pondok Pesantren', '2018-07-24', '88360-ID-sistem-pendukung-keputusan-untuk-menentu.pdf', '2018-08-08 00:09:00', 12, 0),
(44, 'Basmallah', '2018-08-05', 'Distributed System and Security Lab Work 2017.pdf', '2018-08-11 00:00:00', 777, 3);

-- --------------------------------------------------------

--
-- Struktur untuk view `nilai_mahasiswa`
--
DROP TABLE IF EXISTS `nilai_mahasiswa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `nilai_mahasiswa`  AS  select `kotak_masuk`.`nis` AS `nis`,`kotak_masuk`.`nis` AS `niss`,`kotak_masuk`.`nama_siswa` AS `nama_mhs`,`kotak_masuk`.`id_tugas` AS `id_tugas`,`kotak_masuk`.`judul_tugas` AS `judul_tugas`,`kotak_masuk`.`nilai` AS `nilai`,`kotak_masuk`.`status_tugas` AS `status_tugas`,`kotak_masuk`.`jurusan` AS `idJurusan`,`jurusan`.`jurusan` AS `jurusan`,`kelompok`.`id_kelompok` AS `idKelompokUniv`,`kelompok`.`kelompok` AS `namaKelompokUniv`,`kelompok_fk`.`id` AS `idKelompokFk`,`kelompok_fk`.`namaKelompok` AS `namaKelompokFk`,`pendamping_univ`.`id` AS `idPendampingUniv`,`pendamping_univ`.`nama` AS `namaPendampingUniv`,`pendamping_fk`.`id` AS `idPendampingFk`,`pendamping_fk`.`nama` AS `namaPendampingFk`,(select sum(`kotak_masuk`.`nilai`) from (`kotak_masuk` left join `mahasiswa` on((`kotak_masuk`.`nis` = `mahasiswa`.`nim`))) where (`kotak_masuk`.`nis` = `niss`)) AS `total_nilai` from (((((`kotak_masuk` left join `jurusan` on((`kotak_masuk`.`jurusan` = `jurusan`.`id_jurusan`))) left join `kelompok` on((`kotak_masuk`.`id_kelompok` = `kelompok`.`id_kelompok`))) left join `kelompok_fk` on((`kotak_masuk`.`idKelompokFk` = `kelompok_fk`.`id`))) left join `pendamping_univ` on((`kotak_masuk`.`nama_guru` = `pendamping_univ`.`id`))) left join `pendamping_fk` on((`kotak_masuk`.`idPendampungFk` = `pendamping_fk`.`id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idLevel` (`idLevel`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hp`
--
ALTER TABLE `hp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`),
  ADD KEY `idFk` (`idFk`);

--
-- Indexes for table `kelompok`
--
ALTER TABLE `kelompok`
  ADD PRIMARY KEY (`id_kelompok`);

--
-- Indexes for table `kelompok_fk`
--
ALTER TABLE `kelompok_fk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idFk` (`idFk`);

--
-- Indexes for table `kotak_masuk`
--
ALTER TABLE `kotak_masuk`
  ADD PRIMARY KEY (`id_kotakmasuk`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idJurusan` (`idJurusan`),
  ADD KEY `idAkses` (`idAkses`);

--
-- Indexes for table `pendamping_fk`
--
ALTER TABLE `pendamping_fk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idAkses` (`idAkses`),
  ADD KEY `pendamping_fk_ibfk_1` (`idKelompokFk`);

--
-- Indexes for table `pendamping_univ`
--
ALTER TABLE `pendamping_univ`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idAkses` (`idAkses`);

--
-- Indexes for table `penugasan_fk`
--
ALTER TABLE `penugasan_fk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penugasan_fk_ibfk_1` (`idAkses`),
  ADD KEY `idFk` (`idFk`);

--
-- Indexes for table `penugasan_univ`
--
ALTER TABLE `penugasan_univ`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idAkses` (`idAkses`);

--
-- Indexes for table `setting_sistem`
--
ALTER TABLE `setting_sistem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`id_tahun`);

--
-- Indexes for table `tbl_tugas`
--
ALTER TABLE `tbl_tugas`
  ADD PRIMARY KEY (`id_tugas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akses`
--
ALTER TABLE `akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `fakultas`
--
ALTER TABLE `fakultas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `hp`
--
ALTER TABLE `hp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kelompok`
--
ALTER TABLE `kelompok`
  MODIFY `id_kelompok` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `kelompok_fk`
--
ALTER TABLE `kelompok_fk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kotak_masuk`
--
ALTER TABLE `kotak_masuk`
  MODIFY `id_kotakmasuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `pendamping_fk`
--
ALTER TABLE `pendamping_fk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `pendamping_univ`
--
ALTER TABLE `pendamping_univ`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `penugasan_fk`
--
ALTER TABLE `penugasan_fk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `penugasan_univ`
--
ALTER TABLE `penugasan_univ`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `setting_sistem`
--
ALTER TABLE `setting_sistem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tahun`
--
ALTER TABLE `tahun`
  MODIFY `id_tahun` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_tugas`
--
ALTER TABLE `tbl_tugas`
  MODIFY `id_tugas` int(23) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `akses`
--
ALTER TABLE `akses`
  ADD CONSTRAINT `akses_ibfk_1` FOREIGN KEY (`idLevel`) REFERENCES `level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `jurusan`
--
ALTER TABLE `jurusan`
  ADD CONSTRAINT `jurusan_ibfk_1` FOREIGN KEY (`idFk`) REFERENCES `fakultas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelompok_fk`
--
ALTER TABLE `kelompok_fk`
  ADD CONSTRAINT `kelompok_fk_ibfk_1` FOREIGN KEY (`idFk`) REFERENCES `fakultas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`idJurusan`) REFERENCES `jurusan` (`id_jurusan`),
  ADD CONSTRAINT `mahasiswa_ibfk_2` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pendamping_fk`
--
ALTER TABLE `pendamping_fk`
  ADD CONSTRAINT `pendamping_fk_ibfk_1` FOREIGN KEY (`idKelompokFk`) REFERENCES `kelompok_fk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendamping_fk_ibfk_2` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pendamping_univ`
--
ALTER TABLE `pendamping_univ`
  ADD CONSTRAINT `pendamping_univ_ibfk_1` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `penugasan_fk`
--
ALTER TABLE `penugasan_fk`
  ADD CONSTRAINT `penugasan_fk_ibfk_1` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penugasan_fk_ibfk_2` FOREIGN KEY (`idFk`) REFERENCES `fakultas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `penugasan_univ`
--
ALTER TABLE `penugasan_univ`
  ADD CONSTRAINT `penugasan_univ_ibfk_1` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
