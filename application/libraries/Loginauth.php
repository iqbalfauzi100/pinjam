<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loginauth{

  // public $favicon = 'assets/template/img/favicon.png';

  protected $ci;

  public function __construct()
  {
    $this->ci =& get_instance();
  }

  public function view_page()
  {
    if($this->ci->session->userdata('idLevel')=='1'){
      $url =  explode("/",$_SERVER["REQUEST_URI"]);
      if ($url[2]!="admin") {
        redirect('admin/Super_admin');
      }
    }elseif ($this->ci->session->userdata('idLevel')=='2') {
      $url =  explode("/",$_SERVER["REQUEST_URI"]);
      if ($url[2]!="user") {
        redirect('user/User');
      }
    }else{
      $url =  $_SERVER["REQUEST_URI"];
      if ($url!='/pinjam/C_login') {
        redirect('C_login');
      }
    }
  }

}
?>
