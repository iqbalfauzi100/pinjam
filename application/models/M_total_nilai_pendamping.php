<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_total_nilai_pendamping extends CI_Model {

	var $table = 'mahasiswa';//nama table database
	var $select_column = array('mahasiswa.nim','mahasiswa.nama','mahasiswa.idJurusan','mahasiswa.idKelompokU','mahasiswa.idPendampungU','jurusan.jurusan','(pendamping_univ.nama) nama_pendamping');
	var $order_column = array('mahasiswa.nim','mahasiswa.nama','mahasiswa.idJurusan','mahasiswa.idKelompokU','mahasiswa.idPendampungU','jurusan.jurusan','(pendamping_univ.nama) nama_pendamping');
	var $column_search = array('mahasiswa.nim','mahasiswa.nama','mahasiswa.idJurusan','mahasiswa.idKelompokU','mahasiswa.idPendampungU','jurusan.jurusan','pendamping_univ.nama');

	var $default_order = 'mahasiswa.nim';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$id = $this->session->userdata('id');
		$query = "SELECT pendamping_univ.id,pendamping_univ.idKelompok from akses,pendamping_univ where akses.id = pendamping_univ.idAkses and akses.id='$id'";
		$sql   = $this->db->query($query)->row();
		$idPen 		= $sql->id;
		$idKelompok = $sql->idKelompok;

		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$this->db->join('jurusan', 'mahasiswa.idJurusan = jurusan.id_jurusan', 'left');
		$this->db->join('pendamping_univ', 'mahasiswa.idPendampungU = pendamping_univ.id', 'left');
		$this->db->where('mahasiswa.idKelompokU',$idKelompok);
		$this->db->where('mahasiswa.idPendampungU',$idPen);

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST["order"])){
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else{
			$this->db->order_by($this->default_order, 'ASC');
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
	public function view_detail_score($nis)
	{
		$id = $this->session->userdata('id');
		$query = "SELECT pendamping_univ.id,pendamping_univ.idKelompok from akses,pendamping_univ where akses.id = pendamping_univ.idAkses and akses.id='$id'";
		$sql   = $this->db->query($query)->row();
		$idPen 		= $sql->id;
		$idKelompok = $sql->idKelompok;

		$this->db->from('kotak_masuk');
		$this->db->where('nis',$nis);
		$this->db->where('status_tugas',0);
		$this->db->where('keterangan',1);
		$this->db->where('id_kelompok',$idKelompok);
		$this->db->where('nama_guru',$idPen);
		$query = $this->db->get();
		return $query->result();

		$query = null;
		unset($nis);
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id_kotakmasuk',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($idAgama)
	{
		$this->db->where('idAgama', $idAgama);
		$this->db->delete($this->table);
	}
	public function change_id($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}
	public function getdata($key)
	{
		$this->db->from('kelompok');
		$this->db->where('kelompok.id_kelompok',$key);
		$query = $this->db->get();
		return $query->row();
	}
	public function change_status($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function count_score($nis)
	{
		$id = $this->session->userdata('id');
		$query = "SELECT pendamping_univ.id,pendamping_univ.idKelompok from akses,pendamping_univ where akses.id = pendamping_univ.idAkses and akses.id='$id'";
		$sql   = $this->db->query($query)->row();
		$idPen 		= $sql->id;
		$idKelompok = $sql->idKelompok;

		$sql = 'SELECT SUM(kotak_masuk.nilai) nilai FROM kotak_masuk WHERE kotak_masuk.keterangan=1 and kotak_masuk.nis='.$nis.' and kotak_masuk.status_tugas=0 and kotak_masuk.id_kelompok='.$idKelompok.' and kotak_masuk.nama_guru='.$idPen.' ';
		$query = $this->db->query($sql);
		return $query->row();
	}

}
