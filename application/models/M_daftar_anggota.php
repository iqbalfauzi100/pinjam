<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_daftar_anggota extends CI_Model {

	var $table = 'siswa,kelompok,guru';
	var $select_column = array('siswa.id_siswa','siswa.nis','siswa.nama','siswa.jurusan','kelompok.kelompok','siswa.id_pendamping','(guru.nama) nama_guru');
	var $order_column = array('siswa.id_siswa','siswa.nis','siswa.nama','siswa.jurusan','kelompok.kelompok','siswa.id_pendamping','(guru.nama) nama_guru',null);
	var $column_search = array('siswa.id_siswa','siswa.nis','siswa.nama','siswa.jurusan','kelompok.kelompok','siswa.id_pendamping','(guru.nama) nama_guru');
	var $default_order = "siswa.id_siswa";
	var $relasi;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$id_kelompok = $this->session->userdata('id_kelompok');
		$this->relasi = 'siswa.id_kelompok = kelompok.id_kelompok and siswa.id_pendamping = guru.id_guru and siswa.id_kelompok='.$id_kelompok.'';
	}

	private function _get_datatables_query()
	{	
		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$this->db->where($this->relasi);

		$i = 0;
    
    foreach ($this->select_column as $item) 
    {
      if($_POST['search']['value'])
        ($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
      $column[$i] = $item;
      $i++;
    }
    
    if(isset($_POST['order']))
    {
      $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      // $this->db->order_by(key($order), $order[key($order)]);
      // $this->db->order_by('tbl_tugas.id_tugas','ASC');
    }
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1){
			$this->db->limit($_POST['length'], $_POST['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($idAgama)
	{
		$this->db->from($this->table);
		$this->db->where('idAgama',$idAgama);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($idAgama)
	{
		$this->db->where('idAgama', $idAgama);
		$this->db->delete($this->table);
	}
	public function change_id($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


}
