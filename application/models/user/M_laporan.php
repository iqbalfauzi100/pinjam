<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_laporan extends CI_Model {
  var $table = "peminjaman";
  var $select_column = array("peminjaman.id", "peminjaman.nim", "peminjaman.nama","peminjaman.tanggal","peminjaman.jam","peminjaman.status","peminjaman.status","jurusan.jurusan","ruang.ruang");
  
  var $order_column = array("peminjaman.id", "peminjaman.nim", "peminjaman.nama","peminjaman.tanggal","peminjaman.jam","peminjaman.status","peminjaman.status","jurusan.jurusan","ruang.ruang",null);
  var $column_search = array("peminjaman.id", "peminjaman.nim", "peminjaman.nama","peminjaman.tanggal","peminjaman.jam","peminjaman.status","peminjaman.status","jurusan.jurusan","ruang.ruang");
  var $default_order = "peminjaman.id";
  
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  private function make_query()
  { 
    $id_sess = $this->session->userdata('username');
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->join('jurusan', 'peminjaman.jurusan = jurusan.id_jurusan', 'left');
    $this->db->join('ruang', 'peminjaman.ruang = ruang.id', 'left');
    // $this->db->where('peminjaman.nim',$id_sess);
    $this->db->where('peminjaman.status',0);
    $this->db->order_by('peminjaman.id','DESC');

    $i = 0;
    foreach ($this->column_search as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
      }
      $i++;
    }

    if(isset($_POST["order"])){
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else{
      $this->db->order_by($this->default_order, 'DESC');
    }
  }

  public function queryPDF()
  { 
    $id_sess = $this->session->userdata('username');
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->join('jurusan', 'peminjaman.jurusan = jurusan.id_jurusan', 'left');
    $this->db->join('ruang', 'peminjaman.ruang = ruang.id', 'left');
    $this->db->order_by('peminjaman.id','DESC');

    $query = $this->db->get();
    return $query->result();
  }

  public function queryPDF_peminjaman()
  { 
    $id_sess = $this->session->userdata('username');
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->join('jurusan', 'peminjaman.jurusan = jurusan.id_jurusan', 'left');
    $this->db->join('ruang', 'peminjaman.ruang = ruang.id', 'left');
    $this->db->where('peminjaman.status',0);
    $this->db->order_by('peminjaman.id','DESC');

    $query = $this->db->get();
    return $query->result();
  }

  public function queryPDF_pengembalian()
  { 
    $id_sess = $this->session->userdata('username');
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->join('jurusan', 'peminjaman.jurusan = jurusan.id_jurusan', 'left');
    $this->db->join('ruang', 'peminjaman.ruang = ruang.id', 'left');
    $this->db->where('peminjaman.status',1);
    $this->db->order_by('peminjaman.id','DESC');

    $query = $this->db->get();
    return $query->result();
  }

  function make_datatables(){
    $this->make_query();
    if($_POST['length'] != -1){
      $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }
  function get_filtered_data(){
    $this->make_query();
    $query = $this->db->get();
    return $query->num_rows();
  }
  function get_all_data()
  {
    $this->db->select("*");
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
  public function get_by_id_bea($id)
  {
    $this->db->from($this->table);
    $this->db->where('bea.id',$id);
    $query = $this->db->get();
    return $query->row();
  }

  public function get_by_id_user()
  {
    $this->db->from('mahasiswa');
    $this->db->where('mahasiswa.idAkses',$this->session->userdata('id'));
    $query = $this->db->get();
    return $query->row();
  }

  public function change_status_ruang($where, $data)
  {
    $this->db->update('ruang', $data, $where);
    return $this->db->affected_rows();
  }

  function get_ruang($jurusan) {
    $getruang ="select ruang.id,ruang.ruang,ruang.statusPinjam,jurusan.jurusan from jurusan,ruang where jurusan.id_jurusan=ruang.idJurusan AND ruang.statusPinjam=1 and jurusan.id_jurusan='$jurusan' order by jurusan.id_jurusan ASC";
    return $this->db->query($getruang)->result();
  }

  // function get_ruang($jurusan) {
  //   $getruang ="
  //   select ruang.id,ruang.ruang,jurusan.jurusan,
  //   (SELECT peminjaman.nim FROM `peminjaman` WHERE peminjaman.ruang=ruang.id GROUP BY ruang.id) nim
  //   from ruang
  //   LEFT JOIN jurusan ON jurusan.id_jurusan=ruang.idJurusan
  //   LEFT JOIN peminjaman ON peminjaman.ruang=ruang.id
  //   where jurusan.id_jurusan='$jurusan' AND nim NOT IN(SELECT peminjaman.nim FROM `peminjaman` WHERE peminjaman.ruang=ruang.id GROUP BY ruang.id) OR peminjaman.status=1
  //    GROUP BY ruang.id
  //   order by jurusan.id_jurusan ASC
  //   ";
  //   return $this->db->query($getruang)->result();
  // }
}