<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pengembalian extends CI_Model {
  var $table = "peminjaman";
  var $select_column = array("peminjaman.id", "peminjaman.nim", "peminjaman.nama","peminjaman.tanggal","peminjaman.jam","peminjaman.status","peminjaman.status","jurusan.jurusan","ruang.ruang","(ruang.id) idRuang","ruang.statusPinjam");
  
  var $order_column = array("peminjaman.id", "peminjaman.nim", "peminjaman.nama","peminjaman.tanggal","peminjaman.jam","peminjaman.status","peminjaman.status","jurusan.jurusan","ruang.ruang",null);
  var $column_search = array("peminjaman.id", "peminjaman.nim", "peminjaman.nama","peminjaman.tanggal","peminjaman.jam","peminjaman.status","peminjaman.status","jurusan.jurusan","ruang.ruang");
  var $default_order = "peminjaman.id";
  
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  private function make_query()
  { 
    $id_sess = $this->session->userdata('username');
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->join('jurusan', 'peminjaman.jurusan = jurusan.id_jurusan', 'left');
    $this->db->join('ruang', 'peminjaman.ruang = ruang.id', 'left');
    $this->db->where('peminjaman.nim',$id_sess);
    $this->db->order_by('peminjaman.id','DESC');

    $i = 0;
    foreach ($this->column_search as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
      }
      $i++;
    }

    if(isset($_POST["order"])){
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else{
      $this->db->order_by($this->default_order, 'DESC');
    }
  }
  function make_datatables(){
    $this->make_query();
    if($_POST['length'] != -1){
      $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }
  function get_filtered_data(){
    $this->make_query();
    $query = $this->db->get();
    return $query->num_rows();
  }
  function get_all_data()
  {
    $this->db->select("*");
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
  public function get_by_id_bea($id)
  {
    $this->db->from($this->table);
    $this->db->where('bea.id',$id);
    $query = $this->db->get();
    return $query->row();
  }
  function get_ruang($jurusan) {
    $getruang ="select ruang.id,ruang.ruang,jurusan.jurusan from jurusan,ruang where jurusan.id_jurusan=ruang.idJurusan and jurusan.id_jurusan='$jurusan' order by jurusan.id_jurusan ASC";
    return $this->db->query($getruang)->result();
  }

  public function change_status($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }
  public function change_status_ruang($where, $data)
  {
    $this->db->update('ruang', $data, $where);
    return $this->db->affected_rows();
  }
}