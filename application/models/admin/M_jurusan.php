<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jurusan extends CI_Model {
  var $table = "jurusan";
  var $select_column = array("jurusan.id_jurusan", "jurusan.jurusan", "jurusan.idFk","fakultas.namaFk");
  
  var $order_column = array("jurusan.id_jurusan", "jurusan.jurusan", "jurusan.idFk","fakultas.namaFk",null);
  var $column_search = array("jurusan.id_jurusan", "jurusan.jurusan", "jurusan.idFk","fakultas.namaFk");
  var $default_order = "jurusan.id_jurusan";
  
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  private function make_query()
  { 
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->join('fakultas', 'jurusan.idFk = fakultas.id', 'left');
    $this->db->order_by($this->default_order,'DESC');

    $i = 0;
    foreach ($this->column_search as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
      }
      $i++;
    }

    if(isset($_POST["order"])){
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else{
      $this->db->order_by($this->default_order, 'DESC');
    }
  }
  function make_datatables(){
    $this->make_query();
    if($_POST['length'] != -1){
      $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }
  function get_filtered_data(){
    $this->make_query();
    $query = $this->db->get();
    return $query->num_rows();
  }
  function get_all_data()
  {
    $this->db->select("*");
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
  function get_from_fakultas()
  {
    $this->db->select("*");
    $this->db->from('fakultas');
    return $this->db->get()->result();
  }
  public function get_jurusan($fakultas)
  {
   $getjur ="select j.id_jurusan,j.jurusan,f.namaFk from jurusan j,fakultas f where j.idFk=f.id and f.id='$fakultas' order by j.id_jurusan asc";
   return $this->db->query($getjur)->result();
 }
 public function get_by_id($id)
 {
  $this->db->from($this->table);
  $this->db->where('jurusan.id_jurusan',$id);
  $query = $this->db->get();
  return $query->row();
}
function get_ruang($jurusan) {
  $getruang ="select ruang.id,ruang.ruang,jurusan.jurusan from jurusan,ruang where jurusan.id_jurusan=ruang.idJurusan and jurusan.id_jurusan='$jurusan' order by jurusan.id_jurusan ASC";
  return $this->db->query($getruang)->result();
}

public function getInsert($data) {
  $this->db->insert('akses',$data);
  return $this->db->insert_id();
}

public function insertJurusan($data) {
  $this->db->insert('jurusan', $data);
  return $this->db->insert_id();
}

public function getUpdate($where, $data)
{
  $this->db->update('jurusan', $data, $where);
  return $this->db->affected_rows();
}
public function updateMahasiswa($where, $data)
{
  $this->db->update('mahasiswa', $data, $where);
  return $this->db->affected_rows();
}
}