<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_daftar_tugas_penugasan extends CI_Model {
  var $table = "tbl_tugas";
  var $select_column = array("tbl_tugas.id_tugas","tbl_tugas.judul", "tbl_tugas.tgl", "tbl_tugas.berlaku","tbl_tugas.m_file","tbl_tugas.nilai","tbl_tugas.keterangan");
  var $order_column = array("tbl_tugas.id_tugas","tbl_tugas.judul", "tbl_tugas.tgl", "tbl_tugas.berlaku","tbl_tugas.m_file","tbl_tugas.nilai","tbl_tugas.keterangan", null);
  var $column_search = array("tbl_tugas.id_tugas","tbl_tugas.judul", "tbl_tugas.tgl", "tbl_tugas.berlaku","tbl_tugas.m_file","tbl_tugas.nilai","tbl_tugas.keterangan");
  var $relasi;
  var $default_order = "tbl_tugas.id_tugas";
  
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    
  }

  private function _get_datatables_query()
  { 
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->where("tbl_tugas.keterangan",0);

    $i = 0;
    foreach ($this->column_search as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
      }
      $i++;
    }

    if(isset($_POST["order"])){
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else{
      $this->db->order_by($this->default_order, 'DESC');
    }
  }
  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_tugas',$id);
    $query = $this->db->get();

    return $query->row();
  }
  public function delete_by_id($id)
  {
    $this->db->where('id_tugas', $id);
    $this->db->delete($this->table);
  }
  public function getUpdate($where, $data)
  {
    $this->db->update('akses', $data, $where);
    return $this->db->affected_rows();
  }
  public function updatePendampingUniv($where, $data)
  {
    $this->db->update('pendamping_univ', $data, $where);
    return $this->db->affected_rows();
  }
}