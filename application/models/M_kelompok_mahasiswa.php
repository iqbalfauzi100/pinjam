<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kelompok_mahasiswa extends CI_Model {

  var $table = "mahasiswa";
  var $select_column = array("mahasiswa.idAkses", "mahasiswa.nim", "mahasiswa.nama","mahasiswa.idJurusan", "mahasiswa.idKelompokU","mahasiswa.idPendampungU","jurusan.jurusan","kelompok.kelompok","(pendamping_univ.nama) nama_guru");

  var $order_column = array("mahasiswa.idAkses", "mahasiswa.nim", "mahasiswa.nama","mahasiswa.idJurusan", "mahasiswa.idKelompokU","mahasiswa.idPendampungU","jurusan.jurusan","kelompok.kelompok","(pendamping_univ.nama) nama_guru",null);
  var $column_search = array("mahasiswa.idAkses", "mahasiswa.nim", "mahasiswa.nama","mahasiswa.idJurusan", "mahasiswa.idKelompokU","mahasiswa.idPendampungU","jurusan.jurusan","kelompok.kelompok");
  var $default_order = "mahasiswa.id";

  function make_query($kelompok)
  {
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->join('kelompok', 'mahasiswa.idKelompokU = kelompok.id_kelompok', 'left');
    $this->db->join('jurusan', 'mahasiswa.idJurusan = jurusan.id_jurusan', 'left');
    $this->db->join('pendamping_univ', 'mahasiswa.idPendampungU = pendamping_univ.id', 'left');

    //select Berdasarkan kelompok
    if ($kelompok != 0) {
      $this->db->where("mahasiswa.idKelompokU", $kelompok);
    }else{
      
    }
    $i = 0;
    foreach ($this->column_search as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
      }
      $i++;
    }

    if(isset($_POST["order"])){
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else{
      $this->db->order_by($this->default_order, 'ASC');
    }
  }

  function make_datatables($kelompok){
    $this->make_query($kelompok);
    if($_POST['length'] != -1){
      $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function get_filtered_data($kelompok){
    $this->make_query($kelompok);
    $query = $this->db->get();
    return $query->num_rows();
  }

  function get_all_data()
  {
    $this->db->select("*");
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id',$id);
    $query = $this->db->get();

    return $query->row();
  }
  public function get_by_id_santri($id)
  {
    $this->db->from($this->table);
    $this->db->where('id',$id);
    $query = $this->db->get();

    return $query->result();
  }
  public function save($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }
  public function delete_by_id($id)
  {
    $this->db->where('id', $id);
    $this->db->delete($this->table);
  }
  public function change_status($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }
}
