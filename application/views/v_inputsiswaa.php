<?php echo form_open_multipart('super_admin/proses_inputsiswa') ?>
<div class="col-md-12 clearfix">
  <ul id="example-tabs" class="nav nav-tabs" data-toggle="tabs">
    <li class=""><a href="#biodata"></a></li>

  </ul>
<div class="col-sm-12">
      <?php echo $this->session->flashdata('pesan');?></div>
  <div class="tab-content">
    <div class="tab-pane active" id="biodata">
      <table class="table table-bordered">
        <tr class="success"><th colspan="2">INPUT DATA MAHASISWA BARU</th></tr>
        <tr><td width="150">NIM, Nama</td>
          <td>
            <div class='col-sm-2'><input type='text' name='nim' placeholder='No. Induk ..' class='form-control' required='required' value='' ></div>             
            <div class='col-sm-8'><input type='text' name='nama' placeholder='Nama ..' class='form-control' required='required' value='' ></div>
            <tr><td>Password </td><td>
              <div class='col-sm-4'>
                <input type='text' name='password' placeholder='password ..' class='form-control' required='required' value='' >
              </div>                                            </td></tr>                        
              <tr><td>Tahun Angkatan</td><td>
                <div class='col-sm-8'>

                  <select name='tahun'  class='form-control' >
                    <?php 
                    $this->db->from('tahun');
                    $this->db->order_by("tahun", "desc");
                    $sql = $this->db->get(); 
                    foreach ($sql->result() as $key => $value) {
                      ?>
                      <option value='<?php echo $value->tahun ?>'><?php echo $value->tahun ?></option>
                      <?php
                    }

                    ?>



                  </select></div>                                            </td></tr>
                  <tr><td>Kelompok </td><td>
                    <div class='col-sm-8'>

                      <select name='kelas'  class='form-control' >
                        <?php 

                        $sql = $this->db->get('kelompok');
                        foreach ($sql->result() as $key => $value) {
                          ?>
                          <option value='<?php echo $value->id_kelompok ?>'><?php echo $value->kelompok ?></option>



                          <?php
                        }

                        ?>



                      </select>

                    </div>                                            </td></tr>
                    <tr><td>Jurusan</td><td>
                      <div class='col-sm-8'>
                       <select name='jurusan'  class='form-control' >
                        <?php 

                        $sql = $this->db->get('jurusan');
                        foreach ($sql->result() as $key => $value) {
                          ?>
                          <option value='<?php echo $value->jurusan ?>'><?php echo $value->jurusan ?></option>



                          <?php
                        }

                        ?>



                      </select>

                    </div>                                            </td></tr>
                    
                    <!-- <tr>
                      <td>Semester</td>
                      <td>
                        <div class="col-md-8">
                          <select name="semester" class='form-control'>
                            <option value="Ganjil">Ganjil</option>
                            <option value="Genap">Genap</option>
                          </div></td>
                        </tr> -->

                      </div>                                               </div>
                      <tr><td>Gender</td>
                        <td>
                          <div class="col-md-8">
                            <select name="jenis_kelamin" class='form-control'>
                              <option value="Laki - Laki">Laki - Laki</option>
                              <option value="Perempuan">Perempuan</option>
                            </div></td></tr>

                          </div>                                               </div>
                          <div class="col-sm-6">
                           <tr><td width="150"> Foto</td>
                            <td>

                             <div class='col-sm-8'><input type='file' name='userfile' placeholder=' ..' class='form-control' required='required' value='' ></div></td>
                           </tr>                   

                         </table>

                       </div>
                       <input type="submit" name="submit" value="SIMPAN" class="btn btn-danger  btn-sm">
                       <input type="reset"  value="RESET" class="btn btn-danger  btn-sm">
                       <a href="index.php/super_admin/table_siswa" class="btn btn-danger btn-sm">KEMBALI</a></form>


                     </form>