  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
    $( function() {
      $("#datepicker").datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true, 
      });
    } );
  </script>

  <?php echo form_open_multipart('super_admin/proses_addtugas') ?>
  <div class="col-md-12 clearfix">
    <ul id="example-tabs" class="nav nav-tabs" data-toggle="tabs">
      <li class=""><a href="#biodata"></a></li>

    </ul>
  <div class="col-sm-12">
      <?php echo $this->session->flashdata('pesan');?></div>
    <div class="tab-content">
      <div class="tab-pane active" id="biodata">
        <table class="table table-bordered">
          <tr class="success"><th colspan="2">INPUT TUGAS</th>
          </tr>
          <tr>
            <td width="150"> Judul Tugas</td>
            <td>

             <div class='col-sm-8'><input type='text' name='judul' class='form-control' required='required' value='' >
             </div>
           </td>
         </tr>
        <!--  <tr>
             <td width="150"> Nama Pendamping</td>
             <td>
               <div class='col-sm-8'>
                 <input type='text' name='nama_guru' placeholder='Username ..' disabled="disabled" class='form-control' required='required' value='<?php echo $username; ?>' >
                 <input type='hidden' name='nama_guru' placeholder='Username ..' class='form-control' required='required' value='<?php echo $username; ?>' >
               </div>
             </td>
           </tr>  -->   
         
         <!-- <tr>
           <td>Mata Pelajaran</td>
           <td>
            <div class='col-sm-8'>
             <select name='id_matapelajaran'  class='form-control' >
              <?php 
              $sql = $this->db->get('tbl_matapelajaran');
              foreach ($sql->result() as $key => $value) { ?>
              <option value='<?php echo $value->id_matapelajaran ?>'><?php echo $value->matapelajaran ?></option>
              <?php } ?>
            </select>
          </div>
                 </td>
               </tr> -->

               <tr>
                <td width="150"> Berlaku </td>
                <td>
                 <div class='col-sm-8'><input type='text' name='berlaku' id="datepicker"  class='form-control' required='required' value='' ></div>
               </td>
             </tr>  

     <!-- <tr>
       <td width="150"> Kelompok</td>
       <td>
         <div class='col-sm-8'>
           <select name='kelas'  class='form-control' >
            <?php 
            $sql = $this->db->get('kelompok');
            foreach ($sql->result() as $key => $value) { ?>
            <option value='<?php echo $value->kelompok ?>'><?php echo $value->kelompok ?></option>
            <?php } ?>
          </select>
        </div>
      </td>
    </tr> -->
    <!-- <input type="hidden" name='kelas' value="<?php echo $this->session->userdata('id_kelompok');?>"> -->
    <input type="hidden" name="tgl" value="<?php echo date("Y-m-d");?>">
    <tr>
      <td width="150"> File</td>
      <td>
       <div class='col-sm-8'><input type='file' name='userfile' placeholder='Pelajaran ..' class='form-control' required='required' value='' >
         <b style="color:red">* Format file PDF</b>
       </div>
     </td>
   </tr>
 </table>

</div>
<input type="submit" name="submit" value="SIMPAN" class="btn btn-danger  btn-sm">
<input type="reset"  value="RESET" class="btn btn-danger  btn-sm">
</form>

</form>