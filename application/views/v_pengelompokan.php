<div class="col-lg-12">
  <h3 class="page-header">TABEL PENGELOMPOKAN MAHASISWA DENGAN PENDAMPING</h3>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a  class="btn btn-primary btn-sm" href="index.php/super_admin/input_siswa">TAMBAH</a>
      </div>
      <div class="panel-heading">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label">Kelompok</label>
            <div class="col-sm-7">
              <select class="form-control" name="id_kelompok" id="id_kelompok">
                <?php 
                $sql = $this->db->get('kelompok');
                foreach ($sql->result() as $key => $value) { ?>
                <option value="<?php echo $value->id_kelompok ?>"><?php echo $value->kelompok ?></option>
                <?php } ;?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Pendamping</label>
            <div class="col-sm-7">
              <select class="form-control" name="id_pendamping" id="id_pendamping">
                <?php 
                $sql = $this->db->get('guru');
                foreach ($sql->result() as $key => $value) { ?>
                <option value="<?php echo $value->id_guru ?>"><?php echo $value->nama ?></option>
                <?php } ;?>
              </select>
            </div>
          </div>
        </div>
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">
        <div class="dataTable_wrapper">
          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th>NIM</th>
                <th>Nama</th>
                <th>Action</th>
              </tr>   
            </thead>
            <tbody>

            <?php 
            foreach ($mhs as $row) { ?>
              <tr>
                <td><?php echo $row->nis;?></td>
                <td><?php echo $row->nama;?></td>
                <td><label><input type='checkbox' name='check[]' value="<?php echo $row->nis;?>" id="action"></label></td>
              </tr>
              <?php } ;?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    var 
  </script>