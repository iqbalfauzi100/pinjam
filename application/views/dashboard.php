<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-Kunci</title>
    <link rel="shortcut icon" href="assets/images/logo.png" type="image/x-icon" />
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/startbootstrap/vendor/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url('assets/startbootstrap/vendor/fontawesome-free/css/all.min.css')?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="<?php echo base_url('assets/startbootstrap/vendor/magnific-popup/magnific-popup.css')?>" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/startbootstrap/css/freelancer.min.css')?>" rel="stylesheet">

    <!--DataTable-->
    <link href="assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet"/>
    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>
    <style type="text/css"> 
        @media print {
          @page { size: landscape; 
          }

          body * {
            visibility: hidden;
        }
        #diprint, #diprint * {
            visibility: visible;
        }
        #diprint {
            position: absolute;
            left: 0;
            top: 0;
        }
    }
</style>
</head>

<body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">E-Kunci Saintek</a>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Portfolio</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">About</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Data Kunci</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo base_url('C_login')?>" target="_blank">Login</a>
          </li>
      </ul>
  </div>
</div>
</nav>

<!-- Header -->
<header class="masthead bg-primary text-white text-center">
  <div class="container">
    <img class="img-fluid mb-5 d-block mx-auto" src="img/profile.png" alt="">
    <h3 class="text-uppercase mb-0">UIN MAULANA MALIK IBRAHIM MALANG</h3>
    <hr class="star-light">
    <h3 class="font-weight-light mb-0">Web Developer - Fakultas Saintek - UIN Malang</h3>
</div>
</header>

<!-- Portfolio Grid Section -->
<section class="portfolio" id="portfolio">
  <div class="container">
    <h2 class="text-center text-uppercase text-secondary mb-0">Portfolio</h2>
    <hr class="star-dark mb-5">
    <div class="row">
      <div class="col-md-6 col-lg-4">
        <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-1">
          <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
            <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
              <i class="fas fa-search-plus fa-3x"></i>
          </div>
      </div>
      <img class="img-fluid" src="img/portfolio/cabin.png" alt="">
  </a>
</div>
<div class="col-md-6 col-lg-4">
    <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-2">
      <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
        <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
          <i class="fas fa-search-plus fa-3x"></i>
      </div>
  </div>
  <img class="img-fluid" src="img/portfolio/cake.png" alt="">
</a>
</div>
<div class="col-md-6 col-lg-4">
    <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-3">
      <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
        <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
          <i class="fas fa-search-plus fa-3x"></i>
      </div>
  </div>
  <img class="img-fluid" src="img/portfolio/circus.png" alt="">
</a>
</div>
<div class="col-md-6 col-lg-4">
    <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-4">
      <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
        <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
          <i class="fas fa-search-plus fa-3x"></i>
      </div>
  </div>
  <img class="img-fluid" src="img/portfolio/game.png" alt="">
</a>
</div>
<div class="col-md-6 col-lg-4">
    <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-5">
      <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
        <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
          <i class="fas fa-search-plus fa-3x"></i>
      </div>
  </div>
  <img class="img-fluid" src="img/portfolio/safe.png" alt="">
</a>
</div>
<div class="col-md-6 col-lg-4">
    <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-6">
      <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
        <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
          <i class="fas fa-search-plus fa-3x"></i>
      </div>
  </div>
  <img class="img-fluid" src="img/portfolio/submarine.png" alt="">
</a>
</div>
</div>
</div>
</section>

<!-- About Section -->
<section class="bg-primary text-white mb-0" id="about">
  <div class="container">
    <h2 class="text-center text-uppercase text-white">About</h2>
    <hr class="star-light mb-5">
    <div class="row">
      <div class="col-lg-4 ml-auto">
        <p class="lead">Freelancer is a free bootstrap theme created by Start Bootstrap. The download includes the complete source files including HTML, CSS, and JavaScript as well as optional LESS stylesheets for easy customization.</p>
    </div>
    <div class="col-lg-4 mr-auto">
        <p class="lead">Whether you're a student looking to showcase your work, a professional looking to attract clients, or a graphic artist looking to share your projects, this template is the perfect starting point!</p>
    </div>
</div>
<div class="text-center mt-4">
  <a class="btn btn-xl btn-outline-light" href="#">
    <i class="fas fa-download mr-2"></i>
    Download Now!
</a>
</div>
</div>
</section>

<!-- Contact Section -->
<section id="contact">
  <div class="container">
    <h4 class="text-center text-uppercase text-secondary mb-0">Data Peminjaman dan Pengembalian</h4>
    <hr class="star-dark mb-5">

    <div class="row">
        <div class="col-lg-8 mx-auto">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button class="btn btn-success pull-right" onclick="print()"><i class="glyphicon glyphicon-plus"></i> Print All Data</button>&nbsp;&nbsp;
            <button class="btn btn-success pull-right" onclick="print_peminjaman()"><i class="glyphicon glyphicon-plus"></i> Print Data Peminjaman</button>&nbsp;&nbsp;
            <button class="btn btn-success pull-right" onclick="print_pengembalian()"><i class="glyphicon glyphicon-plus"></i> Print Data Pengembalian</button>
        </div>
    </br>
</br>

<!-- <div class="col-lg-10 mx-auto">
   <table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
      <thead>
        <tr class="success">
         <th>No</th>
         <th>Tgl Pinjam</th>
         <th>Jam Pinjam</th>
         <th>NIM</th>
         <th>Peminjam</th>
         <th>Ruangan</th>
         <th>Jurusan</th>
         <th>Status</th>
     </tr>
 </thead>
 <tbody>
 </tbody>
 <tfoot>
  <tr class="success">
    <th>No</th>
    <th>Tgl Pinjam</th>
    <th>Jam Pinjam</th>
    <th>NIM</th>
    <th>Peminjam</th>
    <th>Ruangan</th>
    <th>Jurusan</th>
    <th>Status</th>
</tr>
</tfoot>
</table>
</div> -->


</div>
</div>
</section>

<!-- Footer -->
<footer class="footer text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-4 mb-5 mb-lg-0">
        <h4 class="text-uppercase mb-4">Location</h4>
        <p class="lead mb-0">Jalan Gajayana No.50, Dinoyo, Kecamatan Lowokwaru, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur
          <br>65144</p>
      </div>
      <div class="col-md-4 mb-5 mb-lg-0">
        <h4 class="text-uppercase mb-4">Around the Web</h4>
        <ul class="list-inline mb-0">
          <li class="list-inline-item">
            <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
              <i class="fab fa-fw fa-facebook-f"></i>
          </a>
      </li>
      <li class="list-inline-item">
        <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
          <i class="fab fa-fw fa-google-plus-g"></i>
      </a>
  </li>
  <li class="list-inline-item">
    <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
      <i class="fab fa-fw fa-twitter"></i>
  </a>
</li>
<li class="list-inline-item">
    <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
      <i class="fab fa-fw fa-linkedin-in"></i>
  </a>
</li>
<li class="list-inline-item">
    <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
      <i class="fab fa-fw fa-dribbble"></i>
  </a>
</li>
</ul>
</div>
<div class="col-md-4">
    <h4 class="text-uppercase mb-4">About Me</h4>
    <p class="lead mb-0">Hello My name is Yusuf Adbi Hidigow
      <a href="http://startbootstrap.com">INformatic Engineering</a>.</p>
  </div>
</div>
</div>
</footer>

<div class="copyright py-4 text-center text-white">
  <div class="container">
    <small>Copyright &copy; Your Website 2018</small>
</div>
</div>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-to-top d-lg-none position-fixed ">
  <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
    <i class="fa fa-chevron-up"></i>
</a>
</div>

<!-- Portfolio Modals -->

<!-- Portfolio Modal 1 -->
<div class="portfolio-modal mfp-hide" id="portfolio-modal-1">
  <div class="portfolio-modal-dialog bg-white">
    <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
      <i class="fa fa-3x fa-times"></i>
  </a>
  <div class="container text-center">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2 class="text-secondary text-uppercase mb-0">Project Name</h2>
          <hr class="star-dark mb-5">
          <img class="img-fluid mb-5" src="img/portfolio/cabin.png" alt="">
          <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.</p>
          <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
            <i class="fa fa-close"></i>
            Close Project</a>
        </div>
    </div>
</div>
</div>
</div>

<!-- Portfolio Modal 2 -->
<div class="portfolio-modal mfp-hide" id="portfolio-modal-2">
  <div class="portfolio-modal-dialog bg-white">
    <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
      <i class="fa fa-3x fa-times"></i>
  </a>
  <div class="container text-center">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2 class="text-secondary text-uppercase mb-0">Project Name</h2>
          <hr class="star-dark mb-5">
          <img class="img-fluid mb-5" src="img/portfolio/cake.png" alt="">
          <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.</p>
          <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
            <i class="fa fa-close"></i>
            Close Project</a>
        </div>
    </div>
</div>
</div>
</div>

<!-- Portfolio Modal 3 -->
<div class="portfolio-modal mfp-hide" id="portfolio-modal-3">
  <div class="portfolio-modal-dialog bg-white">
    <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
      <i class="fa fa-3x fa-times"></i>
  </a>
  <div class="container text-center">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2 class="text-secondary text-uppercase mb-0">Project Name</h2>
          <hr class="star-dark mb-5">
          <img class="img-fluid mb-5" src="img/portfolio/circus.png" alt="">
          <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.</p>
          <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
            <i class="fa fa-close"></i>
            Close Project</a>
        </div>
    </div>
</div>
</div>
</div>

<!-- Portfolio Modal 4 -->
<div class="portfolio-modal mfp-hide" id="portfolio-modal-4">
  <div class="portfolio-modal-dialog bg-white">
    <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
      <i class="fa fa-3x fa-times"></i>
  </a>
  <div class="container text-center">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2 class="text-secondary text-uppercase mb-0">Project Name</h2>
          <hr class="star-dark mb-5">
          <img class="img-fluid mb-5" src="img/portfolio/game.png" alt="">
          <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.</p>
          <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
            <i class="fa fa-close"></i>
            Close Project</a>
        </div>
    </div>
</div>
</div>
</div>

<!-- Portfolio Modal 5 -->
<div class="portfolio-modal mfp-hide" id="portfolio-modal-5">
  <div class="portfolio-modal-dialog bg-white">
    <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
      <i class="fa fa-3x fa-times"></i>
  </a>
  <div class="container text-center">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2 class="text-secondary text-uppercase mb-0">Project Name</h2>
          <hr class="star-dark mb-5">
          <img class="img-fluid mb-5" src="img/portfolio/safe.png" alt="">
          <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.</p>
          <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
            <i class="fa fa-close"></i>
            Close Project</a>
        </div>
    </div>
</div>
</div>
</div>

<!-- Portfolio Modal 6 -->
<div class="portfolio-modal mfp-hide" id="portfolio-modal-6">
  <div class="portfolio-modal-dialog bg-white">
    <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
      <i class="fa fa-3x fa-times"></i>
  </a>
  <div class="container text-center">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2 class="text-secondary text-uppercase mb-0">Project Name</h2>
          <hr class="star-dark mb-5">
          <img class="img-fluid mb-5" src="img/portfolio/submarine.png" alt="">
          <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.</p>
          <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
            <i class="fa fa-close"></i>
            Close Project</a>
        </div>
    </div>
</div>
</div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url('assets/startbootstrap/vendor/jquery/jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/startbootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

<!-- Plugin JavaScript -->
<script src="<?php echo base_url('assets/startbootstrap/vendor/jquery-easing/jquery.easing.min.js')?>"></script>
<script src="<?php echo base_url('assets/startbootstrap/vendor/magnific-popup/jquery.magnific-popup.min.js')?>"></script>

<!-- Contact Form JavaScript -->
<script src="<?php echo base_url('assets/startbootstrap/js/jqBootstrapValidation.js')?>"></script>
<script src="<?php echo base_url('assets/startbootstrap/js/contact_me.js')?>"></script>

<!-- Custom scripts for this template -->
<script src="<?php echo base_url('assets/startbootstrap/js/freelancer.min.js')?>"></script>

<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
  var save_method;
  var table;
  $(document).ready(function() {
    table = $('#table').DataTable({ 
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?php echo site_url('C_dashboard/datatable')?>",
        "type": "POST"
    },
    "columnDefs": [
    { 
        "targets": [ -1 ],
        "orderable": false,
    },
    ],

});
});

  function print() {
    window.open("<?=site_url()?>C_dashboard/get_data_print");
}
 function print_peminjaman() {
    window.open("<?=site_url()?>C_dashboard/get_data_print_peminjaman");
}
 function print_pengembalian() {
    window.open("<?=site_url()?>C_dashboard/get_data_print_pengembalian");
}
</script>


</body>

</html>
