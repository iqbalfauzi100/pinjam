<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>LOGIN MAHASISWA</title>
  <base href="<?php echo base_url() ?>" />
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shortcut icon" href="assets/images/logo.png" type="image/x-icon" />
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="assets/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="assets/plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="assets/plugins/morris/morris.css">
  <link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="assets/bootstrap/css/animate.css">
  <link rel="stylesheet" href="assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
</head>
<style type="text/css">
  body{
    background: url('assets/images/bg_siswa.jpg');
  }
</style>
<div class="login-box">
  <div class="login-logo">
    <img src="assets/images/logo.png" width="120px"><br/>
    <a href=".#"><b  style="font-family: Courier new;color: white;">E-Tugas</a>
  </div><!-- /.login-logo -->
  <div class="login-box-body">
    <?php echo  form_open('login_siswa/proses_login') ?>
     <div class="form-group has-feedback animated bounceInLeft" style="animation-delay: 0.5s;">
      <input type="number" required="Harap Di isi" class="form-control" name="nis" placeholder="No. Induk Mahasiswa">

    </div>
     <div class="form-group has-feedback animated bounceInLeft" style="animation-delay: 0.5s;">
      <input type="password" required="Harap Di isi" class="form-control" name="password" placeholder="Password">
    </div>
    <div class="row animated bounceInDown" style="animation-delay: 1s;">
     <div class="col-xs-8">
      <button type="submit" name="login" class="btn btn-primary btn-block btn-flat">Sign In</button>
    </div><!-- /.col -->
    <div class="col-xs-4">
      <a href="index.php" class="btn btn-danger btn-block btn-right">Back</a>
    </div><!-- /.col -->
  </div>
</form>
</div><!-- /.login-box-body -->
</div><!-- /.login-box -->
<style type="text/css">
  footer{
    text-align: center;
    color: white;
  }
</style>
<footer>
  <b> Hakcipta &copy; 2018 Bagian Kemahasiswaan dan Alumni UIN Malang</a></b>
</footer> >