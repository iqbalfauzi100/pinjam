<?php 
$sql = "SELECT mahasiswa.*,(pendamping_univ.nama) nama_guru FROM mahasiswa,pendamping_univ where mahasiswa.idPendampungU=pendamping_univ.id and mahasiswa.nim='$nim'";
$rs=$this->db->query($sql);
foreach ($rs->result() as $row) {

} 
//Nama Kelompok
$this->load->model('m_anggota_kelompok','model');
$nama_kelompok = $this->model->getdata($row->idKelompokU)->kelompok;

//Nama Jurusan
$this->db->select('jurusan');
$this->db->from('jurusan');
$this->db->where('id_jurusan',$idJurusan);
$query = $this->db->get()->row();
$jurusan =$query->jurusan;

?>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Kirim Tugas (Universitas)</h3>
          </div>
          <div class="col-sm-12">
            <?php echo $this->session->flashdata('pesan');?></div>
            <form class="form-horizontal" method="post" action="<?php echo base_url('Siswa/proses_kirim_universitas');?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">NIM</label>
                  <div class="col-sm-10">
                    <input type='text' name='nim' disabled class='form-control' required='required' value='<?php echo $nim ?>' >
                  </div>
                  <input type="hidden" name="nim" value="<?php echo $nim ?>">
                  <input type="hidden" name="id_tugas" value="<?php echo $id_tugas ?>">
                  <input type="hidden" name="nilai" value="<?php echo $nilai ?>">
                  <input type="hidden" name="keterangan" value="<?php echo $keterangan ?>">
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input type='text' name='nama_mhs'  class='form-control' disabled value='<?php echo $nama ?>' >
                  </div>
                  <input type='hidden' name='nama_siswa'  class='form-control' required='required' value='<?php echo $nama ?>' >
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Tahun Angkatan</label>
                  <div class="col-sm-10">
                   <input type='text' name='tahun_angkatan' class='form-control' disabled value='<?php echo $tahun ?>' >
                 </div>
                 <input type="hidden" name="tahun_angkatan" value="<?php echo $tahun ?>">
               </div>
               <div class="form-group">
                <label class="col-sm-2 control-label">Jurusan</label>
                <div class="col-sm-10">
                  <input type='text' name='jurusan'  class='form-control' disabled value='<?php echo $jurusan ?>' >
                </div>
                <input type="hidden" name="jurusan" value="<?php echo $row->idJurusan ?>">
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Kelompok (Universitas)</label>
                <div class="col-sm-10">
                  <input type='text' name='kelas' class='form-control' disabled value='<?php echo $nama_kelompok ?>' >
                </div> 
                <input type="hidden" name="id_kelompok" value="<?php echo $row->idKelompokU ?>">
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Pendamping (Universitas)</label>
                <div class="col-sm-10">
                  <input type='text' name='nama_guru'  class='form-control' disabled value='<?php echo $row->nama_guru ?>' >
                </div>
                <input type="hidden" name="nama_guru" value="<?php echo $row->idPendampungU ?>">
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><font color="red"> <b>Deadline *</b></font></label>
                <div class="col-sm-10">
                  <input type='text' name='deadline'  class='form-control' disabled value='<?php echo $berlaku ?>' >
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Judul Tugas</label>
                <div class="col-sm-10">
                  <textarea name="judul" class='form-control' rows="5" cols="80" disabled="disabled"><?php echo $judul ?></textarea>
                </div>
                <input type='hidden' name='judul_tugas'  class='form-control' value='<?php echo $judul?>' >
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Rangkuman Materi</label>
                <div class="col-sm-10">
                  <textarea id="editor1" name="rangkuman" rows="10" cols="80"></textarea>
                </div>
              </div>
              <!-- <div class="form-group">
                <label class="col-sm-2 control-label">File</label>
                <div class="col-sm-10">
                  <input type='hidden' name='userfile1' placeholder=' ..' class='form-control'>
                </div>
              </div> -->
              <input type='hidden' name='userfile1' placeholder=' ..' class='form-control'>
              <div class="form-group">
                <label class="col-sm-2 control-label">File</label>
                <div class="dropzone col-sm-10">
                  <div class="dz-message">
                   <h3> Klik atau Drop gambar/File disini</h3>
                 </div>
               </div>
             </div>
             <div class="row">
               <div class="col-md-2"></div>
               <div class="col-md-10"><font color="red"> <b>Format file * : jpg | png | JPG | jpeg | pdf | docx | doc | xlsx | xls | pptx | ppt</b></font></div>
             </div>
           </div>
           <!-- /.box-body -->
           <div class="box-footer">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="reset" class="btn btn-warning" id="reset">Reset</button>
            <button type="submit" name="submit" class="btn btn-success pull-left">Simpan</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</div>
</section>
<!-- <div class="dropzone">
  <div class="dz-message">
   <h3> Klik atau Drop gambar disini</h3>
 </div>
</div> -->
<script type="text/javascript">

  Dropzone.autoDiscover = false;

  var foto_upload= new Dropzone(".dropzone",{
    url: "<?php echo base_url('Upload/proses_upload') ?>",
    maxFilesize: 2,
    method:"post",
// acceptedFiles:"image/*",
paramName:"userfile",
dictInvalidFileType:"Type file ini tidak dizinkan",
addRemoveLinks:true,
});


//Event ketika Memulai mengupload
foto_upload.on("sending",function(a,b,c){
  a.token=Math.random();
  c.append("token_foto",a.token);
  c.append("nim",$('[name="nim"]').val());
  c.append("id_tugas",$('[name="id_tugas"]').val());

   //Menmpersiapkan token untuk masing masing foto
 });


//Event ketika foto dihapus
foto_upload.on("removedfile",function(a){
  var token=a.token;
  $.ajax({
    type:"post",
    data:{token:token},
    url:"<?php echo base_url('Upload/remove_foto') ?>",
    cache:false,
    dataType: 'json',
    success: function(){
      console.log("Foto terhapus");
    },
    error: function(){
      console.log("Error");

    }
  });
});


</script>