<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <!-- <h1>Selamat Datang di Halaman Kesiswaan</h1> -->

      <h3>Total Nilai Data Tugas Mahasiswa</h3>
      <br />
      <div class="col-sm-12">
        <?php echo $this->session->flashdata('pesan');?></div>
        <div class="box-body" style="overflow-x: auto;">
          <table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
              <tr class="success">
               <th>No</th>
               <th>NIM</th>
               <th>Nama</th>
               <th>Jurusan</th>
               <th>Total Nilai</th>
             </tr>
           </thead>
           <tbody>
           </tbody>
           <tfoot>
            <tr class="success">
             <th>No</th>
             <th>NIM</th>
             <th>Nama</th>
             <th>Jurusan</th>
             <th>Total Nilai</th>
           </tr>
         </tfoot>
       </table>
     </div>
   </div>
 </div>
</section>
<script type="text/javascript">
  var save_method;
  var table;
  var id_kelompok;
  document.addEventListener("DOMContentLoaded", function (event) {
    datatable();
  });

  function viewTabel() {
   id_kelompok = $("#id_kelompok").val();

   datatable();
 }

 function datatable() {
   table = $('#table').DataTable({
    "destroy": true,
    "processing": true,
    "serverSide": true,
    "order": [],
    "ajax":{
     url: "<?php echo base_url('From_walikelas/datatable_total_nilai'); ?>",
     type: "POST"
   },
   "columnDefs": [
   {
    "targets": [2,-1],
    "orderable":false,
  },
  ],
  // "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>'
});
 }
  // $(document).ready(function() {

  // });

  function add_person()
  {
    save_method = 'add';
    $('#form')[0].reset();
    $('#modal_form').modal('show');
    $('.modal-title').text('Tambah Data Agama');
  }

  function edit_person(id)
  {

    $.ajax({
      url : "<?php echo site_url('penugasan/tugas_selesai/ajax_edit/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

        $('[name="id"]').val(data.id_kotakmasuk);
        $('[name="nilai"]').val(data.nilai);

        // $('#modal_form').modal('show');
        $('#modal_form').openModal();
        $('.modal-title').text('UBAH NILAI');
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function reload_table()
  {
    table.ajax.reload(null,false);
  }

  function view_detail_score(nis) {
    var url = "<?php echo site_url('From_walikelas/view_detail_score')?>";
    $.ajax({
      url : url+"/"+nis,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        detail = '';
        no=0;
        total_skor_mhs=0;
        for (var i = 0; i < data.length; i++) {
          no+=1;
          detail += `
          <tr>
            <td>`+no+`</td>
            <td>`+data[i].judul_tugas+`</td>
            <td>`+data[i].nilai+`</td>
          </tr>
          `;
          total_skor_mhs += parseInt(data[i].nilai);
        }
        $('#nim_mhs').html(data[0].nis);
        $('#detail_skor_mhs').html(detail);
        $('#total_skor_mhs').html(total_skor_mhs);
        // $('#modal1').openModal();
        $('#modal1').modal('show');
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data!');
      }
    });
  }


</script>

<!-- ====================================== Untuk Input Mask ======================================= -->
<!-- Select2 -->
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
<!-- InputMask -->
<script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
<script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
<script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
<!-- ================================================================================================ -->

<script>
    $(document).ready(function() {
        $('#table-modal').DataTable({
            responsive: true
        });
    });
</script>

    <!-- Bootstrap modal -->
    <!-- <div class="modal modal-primary"> -->
    <div class="modal fade" id="modal1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Detail nilai NIM : <span id="nim_mhs"></span></h3>
          </div>
          <div class="modal-body form">
          <table class="table table-bordered table-hover" cellspacing="0" width="100%">
             <thead>
              <tr class="success">
                <td>No</td>
                <td>Judul Tugas</td>
                <td>nilai</td>
              </tr>
            </thead>
            <tbody id="detail_skor_mhs">

            </tbody>
            <tfoot>
              <tr class="success">
                <td></td>
                <td>Total</td>
                <td id="total_skor_mhs"></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  <!-- Modal Structure -->
  <!-- <div id="modal1" class="modal"> -->
  <div class="modal modal-primary" id="ok" role="dialog">
   <div class="modal-dialog">
    <div class="modal-content">
      <h4>Detail Skor NIM : <span id="nim_mhs"></span> </h4>
      <hr><br>
      <div class="row">
        <table>
          <style media="screen">
            .fon{
              font-weight: bold;
            }
          </style>
          <thead style="font-weight: bold;">
            <tr>
              <td>No</td>
              <td>Judul</td>
              <td>nilai</td>
            </tr>
          </thead>
          <tbody id="detail_skor_mhs">

          </tbody>
          <tfoot style="font-weight: bold;">
            <tr>
              <td></td>
              <td>Total</td>
              <td id="total_skor_mhs"></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>