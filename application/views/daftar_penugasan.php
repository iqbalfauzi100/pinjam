<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <!-- <h1>Selamat Datang di Halaman Kesiswaan</h1> -->

      <h3>Data Tugas</h3>
      <br />
      <div class="col-sm-12">
        <?php echo $this->session->flashdata('pesan');?></div>
        <div class="box-body" style="overflow-x: auto;">
          <table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
              <tr class="success">
               <th>No</th>
               <th>Judul</th>
               <!-- <th>Kelompok</th> -->
               <th>Tanggal Mulai</th>
               <th>Deadline</th>
               <th>Nilai</th>
               <th>Keterangan</th>
               <th>File</th>
             </tr>
           </thead>
           <tbody>
           </tbody>
           <tfoot>
            <tr class="success">
              <th>No</th>
              <th>Judul</th>
              <!-- <th>Kelompok</th> -->
              <th>Tanggal Mulai</th>
              <th>Deadline</th>
              <th>Nilai</th>
              <th>Keterangan</th>
              <th>File</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  var save_method;
  var table;
  $(document).ready(function() {
    table = $('#table').DataTable({ 
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?php echo site_url('penugasan/penugasan/datatable_daftarTugas')?>",
        "type": "POST"
      },
      "columnDefs": [
      { 
        "targets": [ -1 ],
        "orderable": false,
      },
      ],

    });
  });

  function add_person()
  {
    save_method = 'add';
    $('#form')[0].reset();
    $('#modal_form').modal('show');
    $('.modal-title').text('Tambah Data Agama');
  }

  function edit_person(id)
  {

    $.ajax({
      url : "<?php echo site_url('penugasan/penugasan/ajax_edit/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

        $('[name="id"]').val(data.id_kotakmasuk);
        $('[name="nilai"]').val(data.nilai);

        $('#modal_form').modal('show');
        $('.modal-title').text('Ubah Nilai');
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function reload_table()
  {
    table.ajax.reload(null,false);
  }

  function save()
  {
    var url;
    if(save_method == 'add') 
    {
      url = "<?php echo site_url('kesiswaan/Con_Master_Agama/ajax_add')?>";
    }
    else
    {
      url = "<?php echo site_url('kesiswaan/Con_Master_Agama/ajax_update')?>";
    }
    $.ajax({
      url : url,
      type: "POST",
      data: $('#form').serialize(),
      dataType: "JSON",
      success: function(data)
      {
       $('#modal_form').modal('hide');
       reload_table();
     },
     error: function (jqXHR, textStatus, errorThrown)
     {
      alert('Error adding / update data');
    }
  });
  }

  function delete_person(id)
  {
    if(confirm('Apakah anda yakin ingin menghapus data ini?'))
    {
        // ajax delete data to database
        $.ajax({
          url : "<?php echo site_url('penugasan/penugasan/hapus_tugas')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
                //if success reload ajax table
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                alert('Error deleting data');
              }
            });
      }
    }

  </script>

  <!-- ====================================== Untuk Input Mask ======================================= -->
  <!-- Select2 -->
  <script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
  <!-- InputMask -->
  <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
  <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
  <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
  <!-- ================================================================================================ -->

  <!-- Page script -->
  <script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
        //Money Euro
        $("[data-mask]").inputmask();
      });
    </script>
