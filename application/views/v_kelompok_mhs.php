<div class="col-lg-12">
  <h4 class="page-header">DAFTAR KELOMPOK MAHASISWA (UNIVERSITAS)</h4>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
  <div class="col-sm-12">
    <div class="form-group">
      <label class="col-sm-2 control-label">Kelompok</label>
      <div class="col-sm-10">
        <select class="form-control" name="kelompok" id="kelompok" onChange="viewTabel()">
          <option value="" disabled selected>~ Pilih Kelompok ~</option>
          <?php 
          $sql = $this->db->get('kelompok')->result();
          foreach ($sql as $key) { ?>
          <option value='<?php echo $key->id_kelompok ?>'><?php echo $key->kelompok ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-lg-12">

    <div class="panel panel-default">

      <!-- /.panel-heading -->
      
      <div class="panel-body">
        <div class="dataTable_wrapper">
          <!-- <table class="table table-striped table-bordered table-hover" id="dataTables-example"> -->
          <table id="tabel" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
              <tr class="success">
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Jurusan</th>
                <th>Kelompok</th>
                <th>Nama Pendamping</th>
              </tr>   
            </thead>
            <tbody>
            </tbody>
            <tfoot>
              <tr class="success">
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Jurusan</th>
                <th>Kelompok</th>
                <th>Nama Pendamping</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <script type="text/javascript">
          var save_method;
          var table;
          var dataTable;
          var kelompok;

          document.addEventListener("DOMContentLoaded", function (event) 
          {
            // datatable();
          });

          function viewTabel() {
           kelompok = $("#kelompok").val();
           datatable();
         }

         function datatable() {
           dataTable = $('#tabel').DataTable({
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax":{
             url: "<?php echo site_url('admin/Ci_admin/datatable_kelompok')?>",
             type: "POST",
             data:{'kelompok':kelompok}
           },
           "columnDefs": [
           {
            "targets": [2,-1],
            "orderable":false,
          },
          ],
          "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>',
        });
         }

         function reload_table()
         {
          table.ajax.reload(null,false);
        }

        function save()
        {
          var url;
          if(save_method == 'add') 
          {
            url = "<?php echo site_url('kesiswaan/Con_Master_Agama/ajax_add')?>";
          }
          else
          {
            url = "<?php echo site_url('kesiswaan/Con_Master_Agama/ajax_update')?>";
          }
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
             $('#modal_form').modal('hide');
             reload_table();
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
            alert('Error adding / update data');
          }
        });
        }

        function delete_person(idAgama)
        {
          if(confirm('Apakah kamu yakin ingin menghapus data ini ?'))
          {
            $.ajax({
              url : "<?php echo site_url('kesiswaan/Con_Master_Agama/ajax_delete')?>/"+idAgama,
              type: "POST",
              dataType: "JSON",
              success: function(data)
              {
               $('#modal_form').modal('hide');
               reload_table();
             },
             error: function (jqXHR, textStatus, errorThrown)
             {
              alert('Error adding / update data');
            }
          });

          }
        }

        function seleksi(userId, status)
        {
          var url = "<?php echo site_url('admin/Ci_admin/changeStatus')?>";
          $.ajax({
            url : url+"/"+userId+"/"+status,
            type: "POST",
            data: $('#formInput').serialize(),
            dataType: "JSON",
            success: function(data)
            {
              reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Error adding/update data');
            }
          });
        }

      </script>