<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Uji COba</h3>
          </div>
          <div class="col-sm-12">
            <?php echo $this->session->flashdata('pesan');?></div>
            <form class="form-horizontal" method="post" action="<?php echo base_url('index.php/siswa/call');?>">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label"><font color="red"> <b>Enter Number *</b></font></label>
                  <div class="col-sm-10">
                    <input type='text' name='mobile' placeholder="No" class='form-control' required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Enter Messages</label>
                  <div class="col-sm-10">
                    <textarea id="message" name="message" rows="10" cols="80" required="required"></textarea>
                  </div>
                </div>
                <!-- checkbox -->
                <div class="box-body" style="overflow-x: auto;">
                  <table id="dt-example" class="table table-bordered table-hover" cellspacing="0" width="100%">
                    <thead>
                      <tr class="success">
                       <th><input type="checkbox" onClick="toggle(this)" /> All</th>
                       <th>Nama</th>
                       <th>Nomor</th>
                     </tr>
                   </thead>
                   <tbody>
                    <?php 
                    $hp = $this->db->get('hp')->result();
                    foreach ($hp as $row): ?>
                    <tr>
                      <td><input type="checkbox" name="hp[]" class="checkbox" value="<?php echo $row->no;?>"></td>
                      <td><?php echo $row->nama;?></td>
                      <td><?php echo $row->no;?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
                <tfoot>
                  <tr class="success">
                    <th><input type="checkbox" onClick="toggle(this)" /> All</th>
                    <th>Nama</th>
                    <th>Nomor</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="reset" class="btn btn-warning" id="reset">Reset</button>
            <button type="submit" name="submit" class="btn btn-success pull-left" value="Send Messages">Send Messages</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</div>
</section>
<script>
  $(document).ready(function() {
    $('#dt-example').DataTable({
      responsive: true
    });
  });

  function toggle(source) {
    checkboxes = document.getElementsByName('hp[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }
  }
</script>