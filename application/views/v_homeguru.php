<?php
$id =  $this->session->userdata('id');

$query = "SELECT pendamping_univ.id,pendamping_univ.idKelompok from akses,pendamping_univ where akses.id = pendamping_univ.idAkses and akses.id='$id'";
$sql   = $this->db->query($query)->row();

$idPendamping = $sql->id;
$idKelompok = $sql->idKelompok;

?>
<section class="content-header">
  <h1>
    <!-- Selamat datang, <?php echo $username;?> -->
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <?php
          $this->db->from('mahasiswa');
          $this->db->where('idKelompokU',$idKelompok);
          $this->db->where('idPendampungU',$idPendamping);
          $count = $this->db->get()->num_rows();
          ;?>
          <h3><?php echo $count;?></h3>

          <p>Anggota Kelompok</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="From_walikelas/daftar_anggota" class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
         <?php
         $this->db->select('id_tugas');
         $this->db->from('tbl_tugas');
         $this->db->where('tbl_tugas.keterangan',0);
         $count_tugas = $this->db->get()->num_rows();
         ;?>
         <h3><?php echo $count_tugas;?></h3>

         <p>Daftar Tugas</p>
       </div>
       <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
      <a href="From_walikelas/daftar_tugas" class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
      <div class="inner">
       <?php
       $this->db->from('kotak_masuk');
       $this->db->where(array('id_kelompok'=>$idKelompok,'nama_guru'=>$idPendamping,'status_tugas'=>0));
       $count = $this->db->get()->num_rows();
       ;?>
       <h3><?php echo $count;?></h3>

       <p>Kotak Masuk (Verifikasi Tugas)</p>
     </div>
     <div class="icon">
      <i class="ion ion-person-add"></i>
    </div>
    <a href="From_walikelas/kotak_masuk" class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
  </div>
</div>
</div>