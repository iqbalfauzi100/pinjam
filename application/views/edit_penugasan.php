  <script type="text/javascript">
    $(function () {
      $('#datetimepicker1').datetimepicker({
        format : 'YYYY-MM-DD HH:mm:ss'
      });
    });
  </script>
    <script type="text/javascript">
    $(function () {
      $('#datetimepicker2').datetimepicker({
        format : 'YYYY-MM-DD HH:mm:ss'
      });
    });
  </script>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Penugasan</h3>
            </div>
            <div class="col-sm-12">
              <?php echo $this->session->flashdata('pesan');?>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url('penugasan/penugasan/proses_updatetugas');?>" enctype="multipart/form-data">
              <div class="box-body">
                <?php foreach ($data->result() as $row) {
                  ?>
                  <input type='hidden' name='id_tugas'  class='form-control' value='<?php echo $row->id_tugas; ?>'>
                  <div class="form-group">
                    <label for="nama" class="col-sm-2 control-label">Judul Tugas</label>
                    <div class="col-sm-10">
                      <input type='text' name='judul' class='form-control' required='required' value="<?php echo $row->judul?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Tanggal Mulai</label>
                    <div class="col-sm-10">
                     <input type='text' name='mulai' class='form-control' required='required' id='datetimepicker1' value="<?php echo $row->tgl?>">
                   </div>
                 </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Deadline</label>
                    <div class="col-sm-10">
                     <input type='text' name='berlaku' class='form-control' required='required' id='datetimepicker2' value="<?php echo $row->berlaku?>">
                   </div>
                 </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Nilai</label>
                    <div class="col-sm-10">
                     <input type="number" name="nilai" class="form-control" required="required" maxlength="3" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" value="<?php echo $row->nilai?>">
                   </div>
                 </div>
                 <div class="form-group">
                  <label class="col-sm-2 control-label">File</label>
                  <div class="col-sm-10">
                    <input type='file' name='userfile' placeholder=' ..' class='form-control'>
                    <span><font color="red">* Format file PDF</font></span>
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="reset" class="btn btn-warning" id="reset">Reset</button>
                <button type="submit" name="submit" class="btn btn-success pull-left">Simpan</button>
              </div>
              <!-- /.box-footer -->
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>