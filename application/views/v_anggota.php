<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <!-- <h1>Selamat Datang di Halaman Kesiswaan</h1> -->

      <h3>Tambah Anggota Kelompok <?php echo $nama_kelompok;?></h3>
      <br />
      <div class="box-body" style="overflow-x: auto;">
        <table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
          <thead>
            <tr class="success">
             <th>No.</th>
             <th>NIM</th>
             <th>Nama</th>
             <th>Jurusan</th>
             <th>Id Kel</th>
             <th>Nama Kelompok</th>
             <th style="width:125px;">Aksi</th>
           </tr>
         </thead>
         <tbody>
         </tbody>
         <tfoot>
          <tr class="success">
            <th>No.</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Jurusan</th>
            <th>Id Kel</th>
            <th>Nama Kelompok</th>
            <th>Aksi</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <!-- <h1>Selamat Datang di Halaman Kesiswaan</h1> -->

      <h3>Daftar Anggota Kelompok</h3>
      <br />
      <div class="box-body" style="overflow-x: auto;">
      <table id="table2" class="table table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
          <tr class="success">
           <th>No.</th>
           <th>NIM</th>
           <th>Nama</th>
           <th>Jurusan</th>
           <th>Nama kelompok</th>
           <th>Nama Pendamping</th>
         </tr>
       </thead>
       <tbody>
       </tbody>
       <tfoot>
        <tr class="success">
          <th>No.</th>
          <th>NIM</th>
          <th>Nama</th>
          <th>Jurusan</th>
          <th>Nama kelompok</th>
          <th>Nama Pendamping</th>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
</div>
</section>
<script type="text/javascript">
  var save_method;
  var table;
  var table2;
  $(document).ready(function() {
    table = $('#table').DataTable({
      "processing":true,
      "serverSide":true,
      "autoWidth"   : false,
      "ajax":{
        "url":"<?php echo site_url('From_walikelas/datatable')?>",
        "type":"POST"
      },
      "columnDefs":[
      {
        "targets":[-1],
        "orderable":false,
      },
      ],
    });

    table2 = $('#table2').DataTable({ 
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?php echo site_url('From_walikelas/datatable2')?>",
        "type": "POST"
      },
      "columnDefs": [
      { 
        "targets": [ -1 ],
        "orderable": false,
      },
      ],

    });
  });

  function add_person()
  {
    save_method = 'add';
    $('#form')[0].reset();
    $('#modal_form').modal('show');
    $('.modal-title').text('Tambah Data Agama');
  }

  function edit_person(idAgama)
  {
    save_method = 'update';
    $('#form')[0].reset();
    $.ajax({
      url : "<?php echo site_url('kesiswaan/Con_Master_Agama/ajax_edit/')?>/" + idAgama,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

        $('[name="idAgama"]').val(data.idAgama);
        $('[name="namaAgama"]').val(data.namaAgama);

        $('#modal_form').modal('show');
        $('.modal-title').text('Edit Data Agama');
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function reload_table()
  {
    table.ajax.reload(null,false);
  }
  function reload_table2()
  {
    table2.ajax.reload(null,false);
  }

  function save()
  {
    var url;
    if(save_method == 'add') 
    {
      url = "<?php echo site_url('kesiswaan/Con_Master_Agama/ajax_add')?>";
    }
    else
    {
      url = "<?php echo site_url('kesiswaan/Con_Master_Agama/ajax_update')?>";
    }
    $.ajax({
      url : url,
      type: "POST",
      data: $('#form').serialize(),
      dataType: "JSON",
      success: function(data)
      {
       $('#modal_form').modal('hide');
       reload_table();
     },
     error: function (jqXHR, textStatus, errorThrown)
     {
      alert('Error adding / update data');
    }
  });
  }

  function delete_person(idAgama)
  {
    if(confirm('Apakah kamu yakin ingin menghapus data ini ?'))
    {
      $.ajax({
        url : "<?php echo site_url('kesiswaan/Con_Master_Agama/ajax_delete')?>/"+idAgama,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
         $('#modal_form').modal('hide');
         reload_table();
       },
       error: function (jqXHR, textStatus, errorThrown)
       {
        alert('Error adding / update data');
      }
    });
      
    }
  }

  function seleksi(userId, status)
  {
    var url = "<?php echo site_url('From_walikelas/addAnggota')?>";
    $.ajax({
      url : url+"/"+userId+"/"+status,
      type: "POST",
      data: $('#formInput').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        reload_table();
        reload_table2();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding/update data');
      }
    });
  }

</script>

<!-- ====================================== Untuk Input Mask ======================================= -->
<!-- Select2 -->
<script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
<!-- InputMask -->
<script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
<script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
<script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
<!-- ================================================================================================ -->

<!-- Page script -->
<script>
  $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
        //Money Euro
        $("[data-mask]").inputmask();
      });
    </script>