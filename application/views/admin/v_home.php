  <section class="content-header">
    <h1>
      Selamat datang, <?php echo $this->session->userdata('username'); ;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <?php 
            $count_mhs = $this->db->get('mahasiswa')->num_rows();
            ?>
            <h3><?php echo $count_mhs;?></h3>

            <p>Mahasiswa</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <?php
            $this->db->from('jurusan');
            $count_jrs = $this->db->get()->num_rows();
            ?>
            <h3><?php echo $count_jrs;?></h3>
            <p>Jurusan</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="admin/Jurusan" class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
           <?php
            $this->db->from('ruang');
            $count_ruang = $this->db->get()->num_rows();
            ?>
            <h3><?php echo $count_ruang;?></h3>

           <p>Ruangan</p>
         </div>
         <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="admin/Ruangan" class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
  <!-- <div class="col-lg-3 col-xs-6">
    small box
    <div class="small-box bg-red">
      <div class="inner">
        <h3>65</h3>
  
        <p>Unique Visitors</p>
      </div>
      <div class="icon">
        <i class="ion ion-pie-graph"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div> -->
  <!-- ./col -->
</div>