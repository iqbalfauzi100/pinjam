  <script type="text/javascript">
    $(function () {
      $('#datetimepicker1').datetimepicker({
        format : 'YYYY-MM-DD'
        // format : 'YYYY-MM-DD HH:mm:ss'
      });
    });
  </script>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Data User Sistem - Mahasiswa</h3>
            </div>
            <div class="col-xs-12">
              <button class="btn btn-success pull-right" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i> Tambah data</button>
            </div>
          </br>
          <div class="box-body" style="overflow-x: auto;">
            <table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
              <thead>
                <tr class="success">
                 <th>No</th>
                 <th>NIM</th>
                 <th>Nama</th>
                 <th>J.Kelamin</th>
                 <th>Jurusan</th>
                 <th>Password</th>
                 <th>Aksi</th>
               </tr>
             </thead>
             <tbody>
             </tbody>
             <tfoot>
              <tr class="success">
                <th>No</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>J.Kelamin</th>
                <th>Jurusan</th>
                <th>Password</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<script type="text/javascript">
  var save_method;
  var table;
  $(document).ready(function() {
    table = $('#table').DataTable({ 
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?php echo site_url('admin/Admin/datatable')?>",
        "type": "POST"
      },
      "columnDefs": [
      { 
        "targets": [ -1 ],
        "orderable": false,
      },
      ],

    });
  });

  
  function add_person()
  {
    save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah Data Mahasiswa'); // Set Title to Bootstrap modal title
    }

    function edit_person(no_induk)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('admin/Admin/edit/')?>/" + no_induk,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          document.getElementById("nim").readOnly = true;

          $('[name="nim"]').val(data.nim);
          $('[name="nama"]').val(data.nama);
          $('[name="jk"]').val(data.jenis_kelamin);
          $('[name="password"]').val(data.password);

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Data Siswa'); // Set title to Bootstrap modal title
            
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function save()
    {
      var url;
      if(save_method == 'add') 
      {
        url = "<?php echo site_url('admin/Admin/simpan')?>";
      }
      else
      {
        url = "<?php echo site_url('admin/Admin/update')?>";
      }

       // ajax adding data to database
       $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
             },
             error: function (jqXHR, textStatus, errorThrown)
             {
              alert('Error adding / update data');
            }
          });
     }

     function delete_person(no_induk)
     {
      if(confirm('Apakah kamu yakin ingin menghapus data ini ?'))
      {
        // ajax delete data to database
        $.ajax({
          url : "<?php echo site_url('admin/Admin/hapus')?>/"+no_induk,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
             },
             error: function (jqXHR, textStatus, errorThrown)
             {
              alert('Error adding / update data');
            }
          });

      }
    }
  </script>

  <!-- Bootstrap modal -->
  <div class="modal fade modal-primary" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3 class="modal-title">Form Data Mahasiswa</h3>
        </div>
        <div class="modal-body form">
          <form action="#" id="form" class="form-horizontal">         

            <div class="form-body">
              <div class="form-group">           
                <label class="control-label col-md-3">NIM</label>
                <div class="col-md-9">
                  <input name="nim" id="nim" placeholder="NIM" class="form-control" type="text">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3">Nama Lengkap</label>
                <div class="col-md-9">
                  <input name="nama" placeholder="Nama Lengkap" class="form-control" type="text">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3">Jenis Kelamin</label>
                <div class="col-md-9">
                  <select name="jk" class="form-control">
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Fakultas</label>
                <div class="col-md-9">
                  <select name="fakultas" id="fakultas" class="form-control">
                    <option value="" disabled selected>Pilih Fakultas</option>
                    <?php
                    foreach ($fakultas as $row) { ?>
                    <option value='<?php echo $row->id ?>'><?php echo $row->namaFk ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3">Jurusan</label>
                <div class="col-md-9">
                  <select name="jurusan" id="jurusan" class="form-control">
                    <option value="" disabled selected>Pilih Jurusan</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Password</label>
                <div class="col-md-9">
                  <input name="password" placeholder="Password" class="form-control" type="text">
                </div>
              </div>

            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  <script type="text/javascript">
    $(document).ready(function(){
     $('#fakultas').change(function(){
      var fakultas =  $('#fakultas').val();
      $.ajax({
        url: '<?php echo site_url('admin/Admin/getJurusan'); ?>',
        type: 'GET',
        data: "fakultas="+fakultas,
        dataType: 'json',
        success: function(data){
         var fakultas=`<select id="jurusan" name="jurusan">
         <option value="null">Pilih Jurusan</option>`;
         for (var i = 0; i < data.length; i++) {
          fakultas+='<option value="'+data[i].id_jurusan+'">'+data[i].jurusan+'</option>';
        }
        fakultas+=`</select>
        <label>Jurusan</label>`;
        $('#jurusan').html(fakultas);
      }
    });
    });
   });
 </script>