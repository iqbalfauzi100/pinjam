  <script type="text/javascript">
    $(function () {
      $('#datetimepicker1').datetimepicker({
        format : 'YYYY-MM-DD'
        // format : 'YYYY-MM-DD HH:mm:ss'
        // format : 'HH:mm:ss'
      });
    });
  </script>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Input peminjaman kunci fakultas</h3>
            </div>
            <div class="col-sm-12">
              <?php echo $this->session->flashdata('pesan');?>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url('user/User/savePeminjaman');?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">Jurusan</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="jurusan" name="jurusan">
                      <option value="" disabled selected>Pilih Jurusan</option>
                      <?php
                      $jurusan = $this->db->get('jurusan')->result();
                      foreach ($jurusan as $row) { ?>
                      <option value="<?php echo $row->id_jurusan;?>"><?php echo $row->jurusan;?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">Kunci Ruang</label>
                  <div class="col-sm-10" id="ruang">
                    <input type="checkbox" required="required"> None
                  </div>
                </div>

                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">Nama Peminjam</label>
                  <div class="col-sm-10">
                    <input type='text' name='nama' class='form-control' required='required' value="<?php echo $nama_peminjam;?>" readonly>
                  </div>
                </div>
               <!--  <div class="form-group">
                  <label class="col-sm-2 control-label">Tanggal Pinjam</label>
                  <div class="col-sm-10">
                   <input type='text' name='tanggal' class='form-control' required='required' id='datetimepicker1' value="<?php echo date("Y-m-d");?>">
                 </div>
               </div> -->
               <div class="form-group">
                <label for="nama" class="col-sm-2 control-label">Tanggal Pinjam</label>
                <div class="col-sm-10">
                 <?php
                 date_default_timezone_set("Asia/Jakarta");
                 $tanggal_pinjam = date("Y-m-d");
                 $jam_pinjam = date("h:i:sa");
                 ?>
                 <input type='text' name='tanggal' class='form-control' required='required' value="<?php echo $tanggal_pinjam;?>" readonly>
                 <input type='hidden' name='jam' class='form-control' required='required' value="<?php echo $jam_pinjam;?>">
               </div>
             </div>

             <div class="form-group">
              <label for="nama" class="col-sm-2 control-label"></label>
              <div class="col-sm-10" id="ruang2">
                <input type="hidden" required="required">
              </div>
            </div>
            
               <!-- <div class="form-group">
                <label for="nama" class="col-sm-2 control-label">Jam Pinjam</label>
                <div class="col-sm-10">
                  <?php
                  date_default_timezone_set("Asia/Jakarta");
                  $jam_pinjam = date("h:i:sa");
                  ?>
                  <input type='text' name='jam' class='form-control' required='required' value="<?php echo $jam_pinjam;?>" readonly>
                </div>
              </div> -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <button type="reset" class="btn btn-warning" id="reset">Reset</button>
              <button type="submit" name="submit" class="btn btn-success pull-left">Simpan</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Data peminjaman kunci fakultas</h3>
          </div>
          <div class="box-body" style="overflow-x: auto;">
            <table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
              <thead>
                <tr class="success">
                 <th>No</th>
                 <th>Tgl Pinjam</th>
                 <th>Jam Pinjam</th>
                 <th>NIM</th>
                 <th>Peminjam</th>
                 <th>Ruangan</th>
                 <th>Jurusan</th>
                 <th>Status</th>
               </tr>
             </thead>
             <tbody>
             </tbody>
             <tfoot>
              <tr class="success">
                <th>No</th>
                <th>Tgl Pinjam</th>
                <th>Jam Pinjam</th>
                <th>NIM</th>
                <th>Peminjam</th>
                <th>Ruangan</th>
                <th>Jurusan</th>
                <th>Status</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<script type="text/javascript">
  var save_method;
  var table;
  $(document).ready(function() {
    table = $('#table').DataTable({ 
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?php echo site_url('user/User/datatable')?>",
        "type": "POST"
      },
      "columnDefs": [
      { 
        "targets": [ -1 ],
        "orderable": false,
      },
      ],

    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
   $('#jurusan').change(function(){
    var jurusan =  $('#jurusan').val();
    $.ajax({
      url: '<?php echo site_url('user/User/getRuang'); ?>',
      type: 'GET',
      data: "jurusan="+jurusan,
      dataType: 'json',
      success: function(data){

        tampil = '';
        tampil2 = '';
        for (var i = 0; i < data.length; i++) {
          tampil += `
          <input type="checkbox" id="ruang" name="ruang[]" value="`+data[i].id+`"> `+data[i].ruang+`</br>
          `;

          tampil2 += `
          <input type="hidden" id="statusPinjam" name="statusPinjam[]" value="`+data[i].statusPinjam+`"></br>
          `;

        }
        $('#ruang').html(tampil);
        $('#ruang2').html(tampil2);
      }
    });
  });
 });
</script>