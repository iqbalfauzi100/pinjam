  <script type="text/javascript">
    $(function () {
      $('#datetimepicker1').datetimepicker({
        format : 'YYYY-MM-DD'
        // format : 'YYYY-MM-DD HH:mm:ss'
      });
    });
  </script>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Data Pengembalian kunci fakultas</h3>
            </div>
            <div class="box-body" style="overflow-x: auto;">
              <table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
                <thead>
                  <tr class="success">
                   <th>No</th>
                   <th>Tgl Pinjam</th>
                   <th>Jam Pinjam</th>
                   <th>NIM</th>
                   <th>Peminjam</th>
                   <th>Ruangan</th>
                   <th>Jurusan</th>
                   <th>Status</th>
                   <th>Aksi</th>
                 </tr>
               </thead>
               <tbody>
               </tbody>
               <tfoot>
                <tr class="success">
                  <th>No</th>
                  <th>Tgl Pinjam</th>
                  <th>Jam Pinjam</th>
                  <th>NIM</th>
                  <th>Peminjam</th>
                  <th>Ruangan</th>
                  <th>Jurusan</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  var save_method;
  var table;
  $(document).ready(function() {
    table = $('#table').DataTable({ 
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?php echo site_url('user/Pengembalian/datatable')?>",
        "type": "POST"
      },
      "columnDefs": [
      { 
        "targets": [ -1 ],
        "orderable": false,
      },
      ],

    });
  });

  function reload_table()
  {
    table.ajax.reload(null,false);
  }

  function seleksi(userId, status, idRuang, statusPinjam)
  {
    var url = "<?php echo site_url('user/Pengembalian/changeStatus')?>";
    $.ajax({
      url : url+"/"+userId+"/"+status+"/"+idRuang+"/"+statusPinjam,
      type: "POST",
      data: $('#formInput').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        reload_table();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding/update data');
      }
    });
  }
</script>

<script type="text/javascript">
  $(document).ready(function(){
   $('#jurusan').change(function(){
    var jurusan =  $('#jurusan').val();
    $.ajax({
      url: '<?php echo site_url('user/User/getRuang'); ?>',
      type: 'GET',
      data: "jurusan="+jurusan,
      dataType: 'json',
      success: function(data){

        tampil = '';
        for (var i = 0; i < data.length; i++) {
          tampil += `
          <input type="checkbox" id="ruang" name="ruang[]" value="`+data[i].id+`"> `+data[i].ruang+`</br>
          `;
        }
        $('#ruang').html(tampil);
      }
    });
  });
 });
</script>