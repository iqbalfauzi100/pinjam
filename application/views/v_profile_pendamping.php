<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Profile Pendamping</h3>
          </div>
          <div class="col-sm-12">
            <?php echo $this->session->flashdata('pesan');?></div>
            <form class="form-horizontal" method="post" action="<?php echo base_url('From_walikelas/update_profile');?>" enctype="multipart/form-data">
              <div class="box-body">
              <input type="hidden" name="id_guru" value="<?php echo $this->session->userdata('id');?>">
               <div class="form-group">
                <label class="col-sm-2 control-label">Nama</label>
                <div class="col-sm-10">
                  <input type='text' name='nama' placeholder="Nama" class='form-control' required='required' value='<?php echo $nama ?>' >
                </div>
              </div>
               <div class="form-group">                
                <label class="col-sm-2 control-label"><font color="red"> <b>Username *</b></font></label>
                <div class="col-sm-10">
                  <input type='text' name='username' placeholder="Username" class='form-control' required='required' value='<?php echo $username ?>' >
                </div>
              </div>
               <div class="form-group">
                <label class="col-sm-2 control-label"><font color="red"> <b>Password *</b></font></label>
                <div class="col-sm-10">
                  <input type='text' name='password' placeholder="Password" class='form-control' required='required' value='<?php echo $password ?>' >
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Nama Kelompok</label>
                <div class="col-sm-10">
                  <?php 
                  $this->db->from('kelompok');
                  $this->db->where('kelompok.id_kelompok',$idKelompok);
                  $query = $this->db->get()->row();
                  $nama_kelompok =$query->kelompok;
                  ?>
                  <input type='text' name='kelompok'  class='form-control' disabled value='<?php echo $nama_kelompok ?>' >
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Foto</label>
                <div class="col-sm-10">
                  <div class="row">
                    <div class="col-sm-2" align="center">
                      <a href="assets/images/profile_pendamping/<?php echo $foto ?>" target="_blank">
                        <img src="assets/images/profile_pendamping/<?php echo $foto ?>" alt="Foto Pendamping" height="100" width="100"></a>
                      </div>
                      <div class="col-sm-10">
                        <input type='file' name='filefoto' placeholder=' ..' class='form-control'>
                        <span><font color="red">*Max size 200 kb</font></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="reset" class="btn btn-warning" id="reset">Reset</button>
                <button type="submit" name="submit" class="btn btn-success pull-left">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>