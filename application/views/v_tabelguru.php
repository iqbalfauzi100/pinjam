<div class="col-lg-12">
  <h3 class="page-header">TABEL DATA PENDAMPING (UNIVERSITAS)</h3>
</div>
<!-- /.col-lg-12 -->
</div>
<div class="col-sm-12">
  <?php echo $this->session->flashdata('pesan');?></div>
  <!-- /.row -->
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <a  class="btn btn-primary  btn-sm" href="admin/Super_admin/input_guru">TAMBAH</a>

        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
              <thead>
                <tr>
                <th>No</th>
                  <th>Nama Pendamping</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Kelompok</th>
                  <th>Foto</th>
                  <th>Action</th>
                </tr>   
              </thead>
              <tbody>
                <?php 
                $no=1;
                foreach ($data->result() as $row) : ?>

                  <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $row->nama ?></td>
                    <td><?php echo $row->username ?></td>
                    <td><?php echo $row->password ?></td>
                    <td><?php echo $row->kelompok ?></td>
                    <td>
                     <?php   
                     $image = array(
                      'src' => 'assets/images/profile_pendamping/'.($row->foto),
                      'class' => 'photo',
                      'width' => '90',
                      'height' => '30',
                      'rel' => 'lightbox',
                      ); echo img($image); ?>
                    </td>
                    <td>
                      <a href="admin/Super_admin/edit_guru/<?php echo $row->id ?>" title="">
                        <li class="fa fa-pencil-square-o">
                          EDIT
                        </li>
                      </a> &nbsp; &nbsp; 
                      <a href="admin/Super_admin/hapus_guru/<?php echo $row->id ?>" title="">
                       <li class="fa fa-trash-o" onclick="return confirm('Anda Yakin ingin menghapus data ini ?')">
                        HAPUS
                      </li>
                    </a> 
                  </td>
                </tr>
              <?php endforeach; ?>


            </tbody>
          </table>
        </div>
                            <!-- /.table-responsive -->