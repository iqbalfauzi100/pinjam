<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Ruangan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();

		$this->load->library('upload');
		$this->load->helper(array('url'));
		$this->load->model('admin/M_ruangan','model');
		$this->load->model('admin/M_admin','mod');
	}

	public function index()
	{	
		$data['user']=$this->session->userdata('username');
		$data['fakultas'] = $this->model->get_from_fakultas();

		$data['content'] 	 = 	'admin/v_ruangan';
		$this->load->view('admin/view_admin',$data);
	}
	public function user()
	{	
		$data['user'] = $this->session->userdata('username');
		$data['fakultas'] = $this->model->get_from_fakultas();

		$data['content'] 	 = 	'admin/v_user';
		$this->load->view('admin/view_admin',$data);
	}
	public function profile()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'v_profile_penugasan';
		$this->load->view('view_penugasan',$data);
	}
	public function getJurusan() {
		$fakultas = $_GET['fakultas'];
		$getjur = $this->model->get_jurusan($fakultas);
		echo json_encode($getjur);
	}

	public function simpan(){
		$ruang 		= $this->input->post('ruang');
		$jurusan 	= $this->input->post('jurusan');

		$data = array(
			'ruang' 		=> $ruang,
			'idJurusan' 	=> $jurusan,
			'statusPinjam' 	=> 1);
		$this->model->getInsert($data);
		echo json_encode(array("status" => TRUE));
	}
	public function update()
	{
		$id 		= $this->input->post('idRuang');
		$ruang 		= $this->input->post('ruang');
		$jurusan 	= $this->input->post('jurusan');

		$data = array(
			'ruang' 	=> $ruang,
			'idJurusan' => $jurusan);
		$this->model->getUpdate(array('id' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}
	public function hapus($id)
	{
		$this->db->delete('ruang',array('id'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	public function edit($no_induk)
	{
		$data = $this->model->get_by_id($no_induk);
		echo json_encode($data);
	}

	public function update_profile()
	{	
		$id = $this->input->post('idRuang');
		$data = array(
			'username' 	=> $this->input->post('username'),
			'password' 	=> $this->input->post('password')
			);

		$this->db->where('id',$id);
		$this->db->update('akses',$data);
	}
	
	public function datatable()
	{
		$list = $this->model->make_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->ruang;
			$row[] = $model->jurusan;

			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->id."'".')"><i class="glyphicon glyphicon-pencil"></i> </a>
			<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->id."'".')"><i class="glyphicon glyphicon-trash"></i> </a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->model->get_filtered_data(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	
}


