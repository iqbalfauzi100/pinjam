<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();

		$this->load->library('upload');
		$this->load->helper(array('url'));
		$this->load->model('admin/M_admin','model');
	}

	public function index()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'admin/v_home';
		$this->load->view('admin/view_admin',$data);
	}
	public function user()
	{	
		$data['user'] = $this->session->userdata('username');
		$data['fakultas'] = $this->model->get_from_fakultas();

		$data['content'] 	 = 	'admin/v_user';
		$this->load->view('admin/view_admin',$data);
	}
	public function profile()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'v_profile_penugasan';
		$this->load->view('view_penugasan',$data);
	}
	public function getJurusan() {
		$fakultas = $_GET['fakultas'];
		$getjur = $this->model->get_jurusan($fakultas);
		echo json_encode($getjur);
	}

	public function simpan(){

		$nim 		= $this->input->post('nim');
		$password 	= $this->input->post('password');

		$nama 		= $this->input->post('nama');
		$jk 		= $this->input->post('jk');
		$jurusan 	= $this->input->post('jurusan');
		$password 	= $this->input->post('password');

		$data_akses = array(
			'username' 		=> $nim,
			'password' 		=> $password,
			'idLevel'		=> 2);
		$idAkses = $this->model->getInsert($data_akses);

		$data = array(
			'nim' 			=> $nim,
			'nama' 			=> $nama,
			'jenis_kelamin' => $jk,
			'idJurusan'		=> $jurusan,
			'idAkses'		=> $idAkses);
		$Mhs = $this->model->insertMahasiswa($data);
		echo json_encode(array("status" => TRUE));
	}
	public function update()
	{
		$nim 		= $this->input->post('nim');
		$password 	= $this->input->post('password');

		$nama 		= $this->input->post('nama');
		$jk 		= $this->input->post('jk');
		$jurusan 	= $this->input->post('jurusan');
		$password 	= $this->input->post('password');

		$data_akses = array(
			'password' 	=> $password);
		$this->model->getUpdate(array('username' => $nim), $data_akses);

		$data = array(
			'nim' 			=> $nim,
			'nama' 			=> $nama,
			'jenis_kelamin' => $jk,
			'idJurusan'		=> $jurusan);
		$sukses = $this->model->updateMahasiswa(array('nim' => $nim), $data);
		echo json_encode(array("status" => TRUE));
	}
	public function hapus($nim)
	{
		$this->db->delete('akses',array('username'=>$nim));
		$this->db->delete('mahasiswa',array('nim'=>$nim));
		echo json_encode(array("status" => TRUE));
	}

	public function edit($no_induk)
	{
		$data = $this->model->get_by_id($no_induk);
		echo json_encode($data);
	}

	public function update_profile()
	{	
		$id = $this->input->post('id');
		$data = array(
			'username' 	=> $this->input->post('username'),
			'password' 	=> $this->input->post('password')
			);

		$this->db->where('id',$id);
		$this->db->update('akses',$data);

		$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di update</div>");
		redirect('penugasan/penugasan/profile');
	}
	
	public function datatable()
	{
		$list = $this->model->make_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nim;
			$row[] = $model->nama;
			if($model->jenis_kelamin=="L"){
				$row[] = "Laki - Laki";
			}else{
				$row[] = "Perempuan";
			}
			$row[] = $model->jurusan;
			$row[] = $model->password;

			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void()" title="Edit" onclick="edit_person('."'".$model->nim."'".')"><i class="glyphicon glyphicon-pencil"></i> </a>
			<a class="btn btn-sm btn-danger" href="javascript:void()" title="Hapus" onclick="delete_person('."'".$model->nim."'".')"><i class="glyphicon glyphicon-trash"></i> </a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->model->get_filtered_data(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	
}


