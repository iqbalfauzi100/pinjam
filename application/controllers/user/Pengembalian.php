<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Pengembalian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();

		$this->load->library('upload');
		$this->load->helper(array('url'));
		$this->load->model('user/M_pengembalian','model');
	}

	public function index()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'user/v_pengembalian';
		$this->load->view('user/view_user',$data);
	}
	public function peminjaman()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'user/v_input_peminjaman';
		$this->load->view('user/view_user',$data);
	}
	public function getRuang() {
		$jurusan = $_GET['jurusan'];
		$getruang = $this->model->get_ruang($jurusan);
		echo json_encode($getruang);
	}
	public function simpanData($ruang,$jurusan,$nama,$tanggal,$jam)
	{
		$data = array(
			'jurusan'	=> $jurusan,
			'nim'		=> $this->session->userdata('username'),
			'nama' 		=> $nama,
			'tanggal' 	=> $tanggal,
			'jam' 		=> $jam,
			'status' 	=> 1,
			'ruang' 	=> $ruang
			);
		$this->db->insert('peminjaman',$data);
	}
	public function savePeminjaman()
	{
		$jurusan 	= $this->input->post('jurusan');
		$nama		= $this->input->post('nama');
		$tanggal	= $this->input->post('tanggal');
		$jam		= $this->input->post('jam');
		$ruang		= $this->input->post('ruang');

		$count_ruang= count($ruang);
		for ($i=0; $i < $count_ruang; $i++) { 
			$this->simpanData($ruang[$i],$jurusan,$nama,$tanggal,$jam);
		}
		
		$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di simpan</div>");
		redirect('user/User/peminjaman');


	}

	public function profile()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'v_profile_penugasan';
		$this->load->view('view_penugasan',$data);
	}

	public function changeStatus($id, $status, $idRuang, $statusPinjam)
	{
		$change_status;
		if ($status==1) {
			$change_status = 0;
		}else if ($status==0) {
			$change_status = 1;
		}
		$data = array(
			'status' => $change_status
			);

		$change_status_ruang;
		if ($statusPinjam==1) {
			$change_status_ruang = 0;
		}else if ($statusPinjam==0) {
			$change_status_ruang = 1;
		}
		$data_ruang = array(
			'statusPinjam' => $change_status_ruang
			);

		$this->model->change_status(array('id' => $id), $data);
		$this->model->change_status_ruang(array('id' => $idRuang), $data_ruang);
		echo json_encode(array("status" => TRUE));
	}
	
	public function datatable()
	{
		$list = $this->model->make_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->tanggal;
			$row[] = $model->jam;
			$row[] = $model->nim;
			$row[] = $model->nama;
			$row[] = $model->ruang;
			$row[] = $model->jurusan;

			$status = $model->status;
			if($status==1){
				$row[] = '
				<span class="label label-success"><i class="fa fa-clock-o"></i> Kunci di kembalikan</span>
				';
				$row[] = '
				<a class="btn btn-sm btn-success" href="javascript:void()" name="idPengaturan" title="Pinjam Kunci" onclick="seleksi('."'".$model->id."'".','."'".$model->status."'".','."'".$model->idRuang."'".','."'".$model->statusPinjam."'".');"><i class="glyphicon glyphicon-ok"></i> </a>
				';
			}else{
				$row[] = '
				<span class="label label-warning"><i class="fa fa-clock-o"></i> Kunci Sedang dipinjam</span>
				';
				$row[] = '
				<a class="btn btn-sm btn-danger" href="javascript:void()" name="idPengaturan" title="Kembalikan Kunci" onclick="seleksi('."'".$model->id."'".','."'".$model->status."'".','."'".$model->idRuang."'".','."'".$model->statusPinjam."'".');"><i class="glyphicon glyphicon-remove"></i> </a>
				';
			}

			// $row[] = '
			// <a class="btn btn-sm btn-primary" href="assets/images/penugasan/'.$model->m_file.'" title="Lihat File" target="_blank">
			// 	<i class="glyphicon glyphicon-zoom-in"></i> Lihat File
			// </a>
			// <a class="btn btn-sm btn-warning" href="penugasan/penugasan/edit_penugasan/'.$model->id_tugas.'" title="Edit">
			// 	<i class="glyphicon glyphicon-pencil"></i>
			// </a>

			// <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$model->id_tugas."'".')">
			// 	<i class="glyphicon glyphicon-trash"></i>
			// </a>
			// ';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->model->get_filtered_data(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	
}


