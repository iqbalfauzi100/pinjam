<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class Upload extends CI_Controller
{

	// function __construct(){
	// 	parent::__construct();
	// 	$this->load->database();
	// 	$this->load->helper(array('url','file'));
	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 		$this->load->model('m_anggota_kelompok','model');
	// 		$this->load->model('m_item_terkirim','mod');
	// 		$this->load->model('m_daftar_tugas','mdl');
	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}

	// }
	function __construct(){
		parent::__construct();
		// $this->load->library('Loginauth');
		// $this->loginauth->view_page();

		$this->load->database();
		$this->load->helper(array('url','file'));
		$this->load->model('m_anggota_kelompok','model');
		$this->load->model('m_item_terkirim','mod');
		$this->load->model('m_daftar_tugas','mdl');

	}

	function index(){
		$this->load->view('upload_view');
	}


	//Untuk proses upload foto
	function proses_upload(){

		$config['upload_path']   = FCPATH.'/assets/images/kirim_tugas/';
		$config['allowed_types'] = 'jpg|png|JPG|jpeg|pdf|docx|doc|xlsx|xls|pptx|ppt';

		$this->load->library('upload',$config);

		if($this->upload->do_upload('userfile')){

        	//Compress Image
			$config['image_library']='gd2';
			$config['source_image']= FCPATH.'/assets/images/kirim_tugas/'.$this->upload->data('file_name');
			$config['create_thumb']= FALSE;
			$config['maintain_ratio']= FALSE;
			$config['quality']= '50%';
			$config['width']= 1500;
			$config['height']= 1700;
			$config['new_image']= FCPATH.'/assets/images/kirim_tugas/'.$this->upload->data('file_name');
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();

			$gambar = $this->upload->data('file_name');

			$data = array(
				'nim' 		=> $this->input->post('nim'),
				'id_tugas'	=> $this->input->post('id_tugas'),
				'title'		=> $gambar,
				'token'		=> $this->input->post('token_foto')
				);

			$this->db->insert('kotak_masuk_upload',$data);
		}
	}


	//Untuk menghapus foto
	function remove_foto(){
		//Ambil token foto
		$token = $this->input->post('token');
		$foto  = $this->db->get_where('kotak_masuk_upload',array('token'=>$token));


		if($foto->num_rows()>0){
			$hasil = $foto->row();
			$nama_foto = $hasil->title;
			if(file_exists($file=FCPATH.'/assets/images/kirim_tugas/'.$nama_foto)){
				unlink($file);
			}
			$this->db->delete('kotak_masuk_upload',array('token'=>$token));

		}
		echo "{}";
	}

}