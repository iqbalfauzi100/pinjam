<?php defined('BASEPATH')OR exit('tidak ada akses di izinkan');
/**
 *
 */
class C_dashboard extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('user/M_user','model');
		
	}

	public function index()
	{
		$this->load->view('dashboard');
	}

	public function datatable()
	{
		$list = $this->model->make_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->tanggal;
			$row[] = $model->jam;
			$row[] = $model->nim;
			$row[] = $model->nama;
			$row[] = $model->ruang;
			$row[] = $model->jurusan;

			$status = $model->status;
			if($status==0){
				$row[] = '
				<span class="label label-success"><i class="fa fa-clock-o"></i> Kunci dikembalikan</span>
				';
			}else{
				$row[] = '
				<span class="label label-warning"><i class="fa fa-clock-o"></i> Kunci sedang dipinjam</span>
				';
			}
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->model->get_filtered_data(),
			"data" => $data,
			);
		echo json_encode($output);
	}

	public function get_data_print()
	{
		$data['databea'] = $this->model->queryPDF();
		$this->load->view('user/masterPDF', $data);
	}

	public function get_data_print_peminjaman()
	{
		$data['databea'] = $this->model->queryPDF_peminjaman();
		$this->load->view('user/masterPDF_peminjaman', $data);
	}

	public function get_data_print_pengembalian()
	{
		$data['databea'] = $this->model->queryPDF_pengembalian();
		$this->load->view('user/masterPDF_pengembalian', $data);
	}
}
?>
