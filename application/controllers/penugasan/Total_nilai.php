<?php

class Total_nilai extends ci_controller{

	// public function __construct()
	// {
	// 	parent::__construct();
	// 	$this->load->library('upload');
	// 	$this->load->helper(array('url'));
	

	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 		$this->load->model('penugasan/m_total_nilai','model');

	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}
	// }
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();
		$this->load->library('upload');
		$this->load->helper(array('url'));
		$this->load->model('penugasan/m_total_nilai','model');
	}

	public function datatable()
	{
		$id_kelompok = $this->input->post('id_kelompok')?$this->input->post('id_kelompok'):0;
		
		$list = $this->model->get_datatables($id_kelompok);
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nim;
			$row[] = $model->nama;
			$row[] = $model->jurusan;
			$row[] = $model->nama_pendamping;

			$count = $this->model->count_score($model->nim)->nilai;
			$row[] = '<a href="javascript:void(0)" onclick="view_detail_score('.$model->nim.')">'.$count.'</a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->model->count_filtered($id_kelompok),
			"data" => $data,
			);
		echo json_encode($output);
	}
	public function view_detail_score($nis)
	{
		$data = $this->model->view_detail_score($nis);
		echo json_encode($data);
	}
	public function ajax_edit($id)
	{
		$data = $this->model->get_by_id($id);
		echo json_encode($data);
	}
	public function ajax_update()
	{
		$id = $this->input->post('id');
		$data =array(
			'nilai' => $this->input->post('nilai')
			);
		$this->model->update(array('id_kotakmasuk' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}
}