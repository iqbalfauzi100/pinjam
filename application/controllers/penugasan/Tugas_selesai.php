<?php

class Tugas_selesai extends ci_controller{

	// public function __construct()
	// {
	// 	parent::__construct();
	// 	$this->load->library('upload');
	// 	$this->load->helper(array('url'));
		

	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 	$this->load->model('penugasan/m_tugas_selesai','model');
	// 	$this->load->model('m_item_terkirim','mod');

	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}
	// }
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();
		$this->load->library('upload');
		$this->load->helper(array('url'));
		$this->load->model('penugasan/m_tugas_selesai','model');
		$this->load->model('m_item_terkirim','mod');
	}

	public function index(){
		if ($this->session->userdata('username') and $this->session->userdata('password')){
			redirect(base_url('index.php/penugasan/penugasan'));
		}else{
			redirect(base_url('index.php/C_login'));
		}
	}

	public function datatable()
	{
		$id_kelompok = $this->input->post('id_kelompok')?$this->input->post('id_kelompok'):0;
		$id_tugas = $this->input->post('id_tugas')?$this->input->post('id_tugas'):0;
		
		$list = $this->model->get_datatables($id_kelompok,$id_tugas);
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nis;
			$row[] = $model->nama_siswa;
			$row[] = $model->judul_tugas;
			$row[] = $model->pokok_1;
			// $row[] = '
			// <a class="btn btn-sm btn-primary" href="assets/images/kirim_tugas/'.$model->m_file.'" title="Lihat File" target="_blank">
			// 	<i class="glyphicon glyphicon-zoom-in"></i> Lihat File
			// </a>';
			$keterangan = $model->keterangan;
			if($keterangan == 1){
				$row[] = '
				<a class="btn btn-sm btn-primary" onclick="view_detail_upload('.$model->nis.','.$model->id_tugas.')" title="Lihat File" target="_blank">
					<i class="glyphicon glyphicon-zoom-in"></i> Lihat File
				</a>
				<span class="label label-success"><i class="fa fa-clock-o"></i> Verifikasi</span>
				';
			}else{
				$row[] = '
				<a class="btn btn-sm btn-primary" onclick="view_detail_upload('.$model->nis.','.$model->id_tugas.')" title="Lihat File" target="_blank">
					<i class="glyphicon glyphicon-zoom-in"></i> Lihat File
				</a>
				<span class="label label-danger"><i class="fa fa-clock-o"></i> Belum Verifikasi</span>
				';
			}
			$row[] = $model->nilai;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->model->count_filtered($id_kelompok,$id_tugas),
			"data" => $data,
			);
		echo json_encode($output);
	}

		public function view_detail_upload($nim, $id_tugas)
{
  $data = $this->mod->view_detail_upload($nim, $id_tugas);
  echo json_encode($data);
}


	/*$row[] = '<a href="javascript:void(0)" title="Change Nilai" onclick="edit_person('."'".$model->id_kotakmasuk."'".')">'.$model->nilai.'</a>';*/
	
	public function ajax_edit($id)
	{
		$data = $this->model->get_by_id($id);
		echo json_encode($data);
	}
	public function ajax_update()
	{
		$id = $this->input->post('id');
		$data =array(
			'nilai' => $this->input->post('nilai')
			);
		$this->model->update(array('id_kotakmasuk' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}
}